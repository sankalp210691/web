#!/usr/local/bin/python
import argparse
import sys
import os
import json

def readFromPropFile(filename):
    with open (filename, 'r') as f:
        return json.load(f)

networkingApiLocation = "/Users/sankalpkulshrestha/Dropbox/Magikslate/web/web/src/app/services/app-network.service.ts"
projectProps = readFromPropFile("/disk1/web.properties")

buildServerLocationDict = {
        "local": "/usr/local/var/www/",
        "stage": "/usr/share/nginx/html/",
        "prod": "/usr/share/nginx/html/"
}

buildServerEndpointDict = {
        "local": "http://local-core.magikslate.com/",
        "localdev": "http://local-core.magikslate.com/",
        "stage": "https://stage-sg.mgkslt.com/",
        "prod": "https://sg.mgkslt.com/"
}

buildSearchServerEndpointDict = {
	    "local": "http://local-search.magikslate.com/",
        "localdev": "http://local-search.magikslate.com/",
        "stage": "https://stage-search-sg.mgkslt.com/",
        "prod": "https://search-sg.mgkslt.com/"
}

parser = argparse.ArgumentParser()

parser.add_argument('--build', '-b', help='Specify local,stage,prod', type=str, default="localdev")
parser.add_argument('--https', '-https', help='Specify true if host over https. You have to provide certificate', type=str, default="false")
args = parser.parse_args()
buildType = args.build
isSecure = args.https == "true"

def setServerUrl():
    with open (networkingApiLocation, 'r') as file:
        filedata = file.read()
        filedata = filedata.replace("___BASE_SERVER_URL___", buildServerEndpointDict[buildType])
        filedata = filedata.replace("___SEARCH_SERVER_URL___", buildSearchServerEndpointDict[buildType])
	filedata = filedata.replace("___GOOGLE_API_KEY___", projectProps["GOOGLE_API_KEY"])
    with open(networkingApiLocation, 'w') as file:
        file.write(filedata)

def unsetServerUrl():
    with open (networkingApiLocation, 'r') as file:
        filedata = file.read()
        filedata = filedata.replace(buildServerEndpointDict[buildType], "___BASE_SERVER_URL___")
        filedata = filedata.replace(buildSearchServerEndpointDict[buildType], "___SEARCH_SERVER_URL___")
	filedata = filedata.replace(projectProps["GOOGLE_API_KEY"], "___GOOGLE_API_KEY___")
    with open(networkingApiLocation, 'w') as file:
        file.write(filedata)

def localDev():
    setServerUrl()
    if isSecure:
        command = "cd web; ng serve --host 0.0.0.0 --port 4200 --disableHostCheck --ssl --ssl-key /Users/sankalpkulshrestha/rootCA.key --ssl-cert rootCA.pem --aot"
    else:
        command = "cd web; ng serve --host 0.0.0.0 --port 4200  --disableHostCheck --aot"
    print("COMMAND --> " + command)
    os.system(command)
    unsetServerUrl()

def build():
    setServerUrl()
    command = "cd web; npm run build;"
    print("COMMAND --> " + command)
    os.system(command)

def deploy():
    if buildType == "local":
        command = "rm -Rf " + buildServerLocationDict[buildType] + "/*" + ";cp -R web/dist/* " + buildServerLocationDict[buildType]
        print("COMMAND --> " + command)
        os.system(command)
    else:
        print(">>>>>>>>>>>>>> Not copying to dest as buildType = " + buildType)

if buildType == "localdev":
    localDev()
else:
    build()
    deploy()
