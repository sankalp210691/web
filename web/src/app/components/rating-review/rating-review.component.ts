import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { Utils } from '../../Utils';
import { AppNetworkService } from '../../services/app-network.service';
import { AppNotificationService } from '../../services/app-notification.service';

@Component({
    selector: 'app-rating-review',
    templateUrl: './rating-review.component.html',
    styleUrls: ['./rating-review.component.css']
})
export class RatingReviewComponent implements OnInit {

    associationTypes = [
    {
        "id": 1,
        "displayValue": "I study/studied here",
    },
    {
        "id": 2,
        "displayValue": "My child studies/studied here",
    },
    {
        "id": 3,
        "displayValue": "I teach/taught here",
    },
    {
        "id": 4,
        "displayValue": "Not directly associated"
    }
    ]
    
    reviewImage: File
    reviewImageFilePreview
    photo: File

    ratingReview = {
        reviewId: null,
        schoolId: null,
        schoolName: "",
        rating: 0,
        review: "",
        postAnonymously: false,
        selectedAssociationType: null,
        isSelfReview: true
    }

    constructor(public appNetworkService: AppNetworkService,
        public appNotificationService: AppNotificationService,
        public dialogRef: MatDialogRef<RatingReviewComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Object,
        public dialog: MatDialog) {

        this.ratingReview.reviewId = data["reviewId"]
        this.ratingReview.schoolId = data["schoolId"]
        this.ratingReview.schoolName = data["schoolName"]
        this.ratingReview.rating = data["rating"] ? data["rating"] : 0
        this.ratingReview.review = data["review"] ? data["review"] : ""
        this.ratingReview.selectedAssociationType = data["associationType"] ? data["associationType"] : null
        this.ratingReview["userId"] = this.appNetworkService.getUser()["id"]
        this.ratingReview["userName"] = this.appNetworkService.getUser()["name"]
    }

    ngOnInit() {
    }

    setRating(event) {
        this.ratingReview.rating = event
    }

    onReviewImageUpload(event) {
        let file = event.target.files[0]
        if (file) {
            let reader = new FileReader();
            let self = this;
            reader.onload = function(e) {
                self.reviewImage = file;
                self.reviewImageFilePreview = reader.result;
            }
            reader.readAsDataURL(file);
        }
    }

    clearReviewImage(event) {
        this.reviewImage = null;
        this.reviewImageFilePreview = null;
    }

    submitReview(event) {
        if(!this.ratingReview.review || this.ratingReview.review.length < 140) {
            this.appNotificationService.notify("Review must be atleast 140 characters in length", "error")
            return
        }
        if(this.ratingReview.review.length > 1500) {
            this.appNotificationService.notify("Too long review. Please keep it under 1500 characters", "error")
            return
        }
        if(!this.ratingReview.selectedAssociationType) {
            this.appNotificationService.notify("Please tell us aout your relationship with the school", "error")
            return
        }
        var button = event.target
        var originalTargetHtml = button.innerHTML
        Utils.markBusy(button)

        if(this.ratingReview.reviewId) {
            this.appNetworkService.editReview(this.ratingReview.reviewId, this.ratingReview.rating, this.ratingReview.review, this.ratingReview.schoolId, this.ratingReview.selectedAssociationType, false)
            .then(d=> {
                Utils.markActive(button, originalTargetHtml)
                this.appNotificationService.notify("Your review has been updated successfully", "info")
                this.dialogRef.close(this.ratingReview)
            }).catch(error => {
                if(error.status == 401) {
                    this.appNotificationService.notify("You are not authorized to update this review.", "error")
                } else {
                    this.appNotificationService.notifyGenericError()
                }
                Utils.markActive(button, originalTargetHtml)
            })
        } else {
            this.appNetworkService.submitReview(this.ratingReview.rating, this.ratingReview.review, this.ratingReview.schoolId, this.ratingReview.selectedAssociationType, false)
            .then(d=> {
                Utils.markActive(button, originalTargetHtml)
                this.appNotificationService.notify("Your review has been submitted successfully", "info")
                this.dialogRef.close(this.ratingReview)
            }).catch(error => {
                if(error.status == 401) {
                    this.appNotificationService.notify("You have already submitted a review for this school.", "error")
                } else {
                    this.appNotificationService.notifyGenericError()
                }
                Utils.markActive(button, originalTargetHtml)
            })
        }
    }

    close(event) {
        this.dialogRef.close()
    }

}
