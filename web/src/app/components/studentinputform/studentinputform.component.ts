import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { ProfilePicComponent } from '../profilepic/profilepic.component'
import { Parent } from '../../models/Parent'
import { User } from '../../models/User'
import { School } from '../../models/School'
import { Class } from '../../models/Class'
import { Student } from '../../models/Student'
import { Utils } from '../../Utils'
import { AppNotificationService } from '../../services/app-notification.service';
import { AppNetworkService } from '../../services/app-network.service';

@Component({
	selector: 'app-student-input-form',
	templateUrl: './studentinputform.component.html',
	styleUrls: ['./studentinputform.component.css']
})
export class StudentInputFormComponent implements OnInit {

	@Input() student: Student
	@Output('onDone') onDone: EventEmitter<Student> = new EventEmitter<Student>();
	public school: School

	public classSectionObject = {}
	public classStandardArray = []
	public genders = Utils.getGenders()
	private originalStudent: Student
	studentDob: Date

	private appNotificationService: AppNotificationService;
	private appNetworkService: AppNetworkService;

	constructor(
		public dialogRef: MatDialogRef<StudentInputFormComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private ans: AppNotificationService,
		private appns: AppNetworkService) { 

		this.school = data.school;
		this.appNotificationService = ans;
		this.appNetworkService = appns;
		if(data.student) {
			this.student = data.student
			this.studentDob = new Date(this.student.dob)
			this.originalStudent = JSON.parse(JSON.stringify(data.student))
			this.originalStudent.profilePicUrl = this.student.profilePicUrl
			for(var index in this.student.parent) {
				var parent = this.student.parent[index]
				if(parent.user.phone.indexOf("+91-") > -1) {
					parent.user.phone = parent.user.phone.replace("+91-", "")
				}
			}
		} else {
			this.student = new Student()
			var defaultParent = new Parent()
			var user: User = new User()
			defaultParent.user = user;
			var parentList:[Parent] = [defaultParent];
			var clazz: Class = new Class()
			this.student.clazz = clazz
			this.student.parent = parentList
		}

		for(var index in this.school.classList) {
			var cl = this.school.classList[index]
			var sectionArray = this.classSectionObject[cl.standard]
			if(!sectionArray) {
				sectionArray = []
				this.classSectionObject[cl.standard] = sectionArray
				this.classStandardArray.push(cl.standard)
			}
			var obj = {"section": cl.section, "id": cl.id}
			sectionArray.push(obj)
		}
	}

	ngOnInit() {

	}

	assignStudentProfilePic(event) {
		this.student.profilePicUrl = event
	}

	cancel(event) {
		this.dialogRef.close();
	}

	save(event) {
		var button = event.target
		var buttonContent = button.innerHTML
		Utils.markBusy(button)

		this.student.dob = this.studentDob.getFullYear() + "-" + (this.studentDob.getMonth() + 1) + "-" + this.studentDob.getDate()

		var localStudentCopy = JSON.parse(JSON.stringify(this.student))
		localStudentCopy.profilePicUrl = this.student.profilePicUrl

		if(!this.checkStudentData()) {
			Utils.markActive(button, buttonContent)
			this.appNotificationService.notify("Please check the student information section", "danger")
			return
		}

		if(!this.checkParentData()) {
			Utils.markActive(button, buttonContent)
			this.appNotificationService.notify("Please check the parent information section", "danger")
			return
		}

		for(var index in localStudentCopy.parent) {
			var parent = localStudentCopy.parent[index]
			if(parent.user.phone.indexOf("+91-") == -1) {
				parent.user.phone = "+91-" + parent.user.phone
			}
		}

		var self = this
		if(localStudentCopy.id) {
			
			this.appNetworkService.editStudent(localStudentCopy, function(error, json) {
				if(error) {
					if(error.status >= 500) {
						self.appNotificationService.notifyGenericError()
					} else if(error.status >= 400) {
						var error = JSON.parse(error._body).map.error
						self.appNotificationService.notify(error, "danger")
					}
				} else {
					self.student = json["student"]
					self.onDone.emit(self.student)
					self.dialogRef.close(self.student);
					self.appNotificationService.notify("Student edited successfully", "info")
				}
				Utils.markActive(button, buttonContent)
			})
		} else {

			this.appNetworkService.createStudent(localStudentCopy, function(error, json) {
				if(error) {
					if(error.status >= 500) {
						self.appNotificationService.notifyGenericError()
					} else if(error.status >= 400) {
						var error = JSON.parse(error._body).map.error
						self.appNotificationService.notify(error, "danger")
					}
				} else {
					self.student = json["student"]
					self.onDone.emit(self.student)
					self.dialogRef.close(self.student);
					self.appNotificationService.notify("Student created successfully", "info")
				}
				Utils.markActive(button, buttonContent)
			})
		}
	}

	private checkStudentData(): Boolean {
		if(!this.student.name || !this.student.admissionCode || !this.student.gender || !this.student.dob || !this.student.clazz.standard || !this.student.clazz.section) {
			return false
		}
		return true
	}

	private checkParentData(): Boolean {
		if(this.student.parent && this.student.parent.length >= 1 ) {
			for(var index in this.student.parent) {
				var parent = this.student.parent[index]

				if(!parent.user.name || !Utils.validatePhone(parent.user.phone)) {
					return false
				}

				if(parent.user.email && !Utils.validateEmail(parent.user.email)) {
					return false
				}
			}
			return true
		} else {
			return false
		}
	}

	addParent(event) {
		var parent = new Parent()
		var user: User = new User()
		parent.user = user;
		this.student.parent.push(parent)
	}

	removeParent(index) {
		if(index > 0 || this.student.id) {
			this.student.parent.splice(index, 1)
		}
	}

	getFirstName(name: string): string{
		if(name)
			return name.split(" ")[0]
		else
			return ""
	}
}
