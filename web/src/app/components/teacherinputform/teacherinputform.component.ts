import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { ProfilePicComponent } from '../profilepic/profilepic.component'
import { Teacher } from '../../models/Teacher'
import { User } from '../../models/User'
import { School } from '../../models/School'
import { Class } from '../../models/Class'
import { Utils } from '../../Utils'
import { AppNotificationService } from '../../services/app-notification.service';
import { AppNetworkService } from '../../services/app-network.service';

@Component({
	selector: 'app-teacher-input-form',
	templateUrl: './teacherinputform.component.html',
	styleUrls: ['./teacherinputform.component.css']
})
export class TeacherInputFormComponent implements OnInit {

	@Input() teacher: Teacher
	@Output('onDone') onDone: EventEmitter<Teacher> = new EventEmitter<Teacher>();
	public school: School

	private originalTeacher: Teacher

	private appNotificationService: AppNotificationService;
	private appNetworkService: AppNetworkService;

	constructor(
		public dialogRef: MatDialogRef<TeacherInputFormComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private ans: AppNotificationService,
		private appns: AppNetworkService) { 

		this.school = data.school;
		this.appNotificationService = ans;
		this.appNetworkService = appns;
		if(data.teacher) {
			this.teacher = data.teacher
			this.originalTeacher = JSON.parse(JSON.stringify(data.teacher))
			if(this.teacher.user.phone.indexOf("+91-") > -1) {
				this.teacher.user.phone = this.teacher.user.phone.replace("+91-", "")
			}
		} else {
			this.teacher = new Teacher()
			var user: User = new User()
			this.teacher.user = user;
		}
	}

	ngOnInit() {

	}

	cancel(event) {
		if(this.teacher.user.phone.indexOf("+91-") == -1) {
			this.teacher.user.phone = "+91-" + this.teacher.user.phone
		}
		this.dialogRef.close();
	}

	save(event) {
		var button = event.target
		var buttonContent = button.innerHTML
		Utils.markBusy(button)

		if(this.teacher.user.phone.indexOf("+91-") == -1) {
			this.teacher.user.phone = "+91-" + this.teacher.user.phone
		}
		var localTeacherCopy = JSON.parse(JSON.stringify(this.teacher))
		localTeacherCopy.ctClass = localTeacherCopy.ctClass ? localTeacherCopy.ctClass["id"] : null;

		if(!this.checkTeacherData()) {
			Utils.markActive(button, buttonContent)
			this.appNotificationService.notify("Please check the information", "danger")
			return
		}

		var self = this
		if(localTeacherCopy.id) {
			
			this.appNetworkService.editTeacher(localTeacherCopy, function(error, json) {
				if(error) {
					if(error.status >= 500) {
						self.appNotificationService.notifyGenericError()
					} else if(error.status >= 400) {
						var error = JSON.parse(error._body).map.error
						self.appNotificationService.notify(error, "danger")
					}
				} else {
					self.teacher = json["teacher"]
					self.onDone.emit(self.teacher)
					self.dialogRef.close(self.teacher);
					self.appNotificationService.notify("Teacher edited successfully", "info")
				}
				Utils.markActive(button, buttonContent)
			})
		} else {

			this.appNetworkService.createTeacher(localTeacherCopy, function(error, json) {
				if(error) {
					if(error.status >= 500) {
						self.appNotificationService.notifyGenericError()
					} else if(error.status >= 400) {
						var error = JSON.parse(error._body).map.error
						self.appNotificationService.notify(error, "danger")
					}
				} else {
					self.teacher = json["teacher"]
					self.onDone.emit(self.teacher)
					self.dialogRef.close(self.teacher);
					self.appNotificationService.notify("Teacher created successfully", "info")
				}
				Utils.markActive(button, buttonContent)
			})
		}
	}

	private checkTeacherData(): Boolean {
		if(!this.teacher.user.name || !this.teacher.employeeId || !this.teacher.user.phone) {
			return false
		}
		return true
	}
}
