import { Component, OnInit, Inject, Input, EventEmitter, Output } from '@angular/core';
import { Utils } from '../../Utils';
import { AppNotificationService } from '../../services/app-notification.service';
import { AppNetworkService } from '../../services/app-network.service';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { LoginComponent } from '../login/login.component';
import { SettingsComponent } from '../settings/settings.component';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

	isLoggedIn: boolean = false
	showCreateSchoolButton: boolean = false
	profilePic = null
	nameChars = null
	//this is temporary for 1st release... once dashboard comes, wont be needed. remove this and it related code from below, later
	schoolProfileUrl = null;

	constructor(private appNetworkService: AppNetworkService, private router: Router, public dialog: MatDialog) { 
		router.events.subscribe((val) => {
			this.profilePic = appNetworkService.getUser() && appNetworkService.getUser().profilePic ? appNetworkService.getUser().profilePic : null;
			this.isLoggedIn = this.appNetworkService.verifyIfLoggedIn()
			if(val instanceof NavigationEnd && this.appNetworkService.verifyIfLoggedIn()) {
				this.appNetworkService.getUserRoleAsync()
				.then(roleList => {
					if((roleList && roleList["ADMIN"])) {
						//change this back to true
						this.showCreateSchoolButton = false;

						if(roleList["ADMIN"].length > 0) {
							this.schoolProfileUrl = "/school/" + roleList["ADMIN"][0]["school"]["id"];
						}

					} else {
						this.showCreateSchoolButton = false;
					}
				}).catch(ex => {
					console.log(ex)
					this.showCreateSchoolButton = false;
				})
			} else {
				this.showCreateSchoolButton = false;
			}
		});
	}

	showAuthModal() {
		let dialogRef = this.dialog.open(LoginComponent, {
			width: "30vw",
			minWidth: '300px'
		});
	}

	logout() {
		this.appNetworkService.logout()
	}

	settings(event) {
		let dialogRef = this.dialog.open(SettingsComponent, {
			width: "60vw",
			minWidth: '300px'
		});
	}

}
