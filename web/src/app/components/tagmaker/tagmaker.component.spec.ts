import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagmakerComponent } from './tagmaker.component';

describe('TagmakerComponent', () => {
  let component: TagmakerComponent;
  let fixture: ComponentFixture<TagmakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagmakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagmakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
