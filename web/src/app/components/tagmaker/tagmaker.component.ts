import { Component, OnInit, Inject, Input, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent} from '@angular/material';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { SearchFilterPipe } from '../../pipes/SearchFilterPipe';

@Component({
	selector: 'app-tagmaker',
	templateUrl: './tagmaker.component.html',
	styleUrls: ['./tagmaker.component.css']
})
export class TagmakerComponent implements OnInit {

	@Input() tagList:Object[];
	@Input() selectedTagList:Object[];
	@Input() placeholder:string;
	@Input() required:boolean;
	@Input() maxtags:number;
	@Input() allowDuplicates:boolean;
	@Output('onUpdate') onUpdate: EventEmitter<Object[]> = new EventEmitter<Object[]>();

	tagCtrl = new FormControl();
	filteredTags: Observable<string[]>;
	tags = [];
	usedTagsSet = {}

	@ViewChild('tagInput') tagInput: ElementRef;

	constructor(public searchfilter: SearchFilterPipe) {}
	ngOnInit() {
		if(this.selectedTagList) {
			for(var index in this.selectedTagList) {
				var tag = this.selectedTagList[index]

				if(!this.allowDuplicates && this.usedTagsSet[tag["id"]]) {
					continue;
				}
				this.tags.push(tag);
				this.usedTagsSet[tag["id"]] = true;
			}
			this.onUpdate.emit(this.tags)
		}
	}

	remove(tag: string): void {
		const index = this.tags.indexOf(tag);

		if (index >= 0) {
			this.tags.splice(index, 1);
			delete this.usedTagsSet[tag["id"]]
		}
		this.onUpdate.emit(this.tags)
	}

	selected(event: MatAutocompleteSelectedEvent): void {
		let tag = event.option.value;
		if(!this.allowDuplicates && this.usedTagsSet[tag["id"]]) {
			this.tagInput.nativeElement.value = '';
			this.tagCtrl.setValue(null);
			return;
		}
		if(this.maxtags && this.tags.length == this.maxtags) {
			this.tags.splice(0, 1);
		}
		this.tags.push(tag);
		this.usedTagsSet[tag["id"]] = true;
		this.tagInput.nativeElement.value = '';
		this.tagCtrl.setValue(null);
		this.onUpdate.emit(this.tags)
	}
}
