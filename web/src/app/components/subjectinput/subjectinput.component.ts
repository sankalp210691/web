import { Component, OnInit, Directive, Input, Output, EventEmitter } from '@angular/core';
import { UploaderComponent } from '../uploader/uploader.component'
import { SubjectinputformComponent } from '../subjectinputform/subjectinputform.component'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { School } from '../../models/School';
import { Class } from '../../models/Class';
import { Teacher } from '../../models/Teacher';
import { Subject } from '../../models/Subject';
import { Utils } from '../../Utils'
import { AppNotificationService } from '../../services/app-notification.service';
import { AppNetworkService } from '../../services/app-network.service';

@Component({
	selector: 'app-subject-input',
	templateUrl: './subjectinput.component.html',
	styleUrls: ['./subjectinput.component.css']
})
export class SubjectInputComponent implements OnInit {

	@Input() school:School;
	@Output('onUpdate') onUpdate: EventEmitter<Object> = new EventEmitter<Object>();
	public file: File

	mappingDataLoading: Boolean = false
	subjects: Subject[] = []
	teachers: Teacher[] = []

	private appNotificationService: AppNotificationService;
	private appNetworkService: AppNetworkService;

	constructor(public dialog: MatDialog,
		private ans: AppNotificationService,
		private appns: AppNetworkService) { 

		this.appNotificationService = ans;
		this.appNetworkService = appns;
	}

	ngOnInit(){
		this.mappingDataLoading = true
		//get teachers
		var teacherPromise = this.appNetworkService.getTeachersBySchool()
		.then(d => {
			this.teachers = d.json()
			this.mappingDataLoading = false
			this.emitOutputEvent(this.subjects, null)
		})
		.catch(e => {
			this.mappingDataLoading = false
			if(e.status >= 500) {
				this.appNotificationService.notifyGenericError()
			}else if(e.status == 400 ) {
				var error = JSON.parse(e._body).error
				this.appNotificationService.notify(error,"danger")
			} else if(e.status == 401){
				this.appNotificationService.notify("You are not authorized","danger")
			}
		})
	}

	showSubjectForm(event, subjectIndex) {
		if(!this.subjects) {
			this.subjects = []
		}
		let dialogRef = this.dialog.open(SubjectinputformComponent, {
			width: "40vw",
			maxHeight: "75vh",
			disableClose:true,
			data: { subject: subjectIndex != null ? this.subjects[subjectIndex] : null, school: this.school, teachers: this.teachers }
		});

		dialogRef.afterClosed().subscribe(subject => {
			if(subject && (!subjectIndex && subjectIndex != 0)) {
				this.subjects.push(subject)
			} else if (subject) {
				this.subjects[subjectIndex] = subject
			}
			this.onUpdate.emit({'subjects': this.subjects, 'file': null})
		});
	}

	inputTypeChange(type) {
		if(type == 'keyin') {
			this.emitOutputEvent(this.subjects, null)
		} else {
			this.emitOutputEvent(null, this.file)
		}
	}

	onFileUpload(event) {
		this.file = event
		this.emitOutputEvent(null, this.file)
	}

	emitOutputEvent(subjects, file) {
		this.onUpdate.emit({'classInfo': this.subjects, 'file': this.file})
	}
}
