import { Component, OnInit, Directive, Input, Output, EventEmitter } from '@angular/core';
import { School } from '../../models/School';
import { Class } from '../../models/Class';
import { Utils } from '../../Utils'
import { AppNotificationService } from '../../services/app-notification.service';
import { AppNetworkService } from '../../services/app-network.service';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';
import { MatTableModule } from '@angular/material/table';

@Component({
	selector: 'app-class-info',
	templateUrl: './classinfo.component.html',
	styleUrls: ['./classinfo.component.css']
})
export class ClassInfoComponent implements OnInit {

	@Input() clazz:Class;
	@Input('school') school:School;
	@Output('onUpdate') onUpdate: EventEmitter<School> = new EventEmitter<School>();
	@Output('onCancel') onCancel: EventEmitter<any> = new EventEmitter<any>();

	private maxSectionCount = 20
	private minSectionCount = 1
	public sectionCountArray: Number[] = [];
	public classStructure;


	constructor(public appNotificationService: AppNotificationService,  public appNetworkService: AppNetworkService) {
		for(var index = this.minSectionCount; index <= this.maxSectionCount; index++) {
			this.sectionCountArray.push(index)
		}
		this.classStructure = [{
			"levelName": "Preschool",
			"structure": [
			{
				"standard": "Pre-Nursery",
				"selected": false,
				"sectionCount": 1
			},
			{  
				"standard": "Nursery",
				"selected": false,
				"sectionCount": 1
			},
			{
				"standard": "U.K.G",
				"selected": false,
				"sectionCount": 1
			},
			{
				"standard": "L.K.G",
				"selected": false,
				"sectionCount": 1
			}
			]
		}];
		this.classStructure.push({
			"levelName": "Middle School",
			"structure": [
			{
				"standard": "1st",
				"selected": false,
				"sectionCount": 1
			},
			{
				"standard": "2nd",
				"selected": false,
				"sectionCount": 1
			},
			{
				"standard": "3rd",
				"selected": false,
				"sectionCount": 1
			}
			]
		})
		for(var index = 4; index <= 5; index++) {
			this.classStructure[1].structure.push({
				"standard": index + "th",
				"selected": false,
				"sectionCount": 1
			})
		}
		this.classStructure.push({
			"levelName": "High School",
			"structure": []
		})
		for(var index = 6; index <= 12; index++) {
			this.classStructure[2].structure.push({
				"standard": index + "th",
				"selected": false,
				"sectionCount": 1
			})
		}
	}

	cancel(event) {
		this.onCancel.emit(event)
	}

	saveClass(event) {
		var reqBody = [];
		for(var index in this.classStructure) {
			var cs = this.classStructure[index];
			for(var subindex in cs.structure) {
				var st = cs.structure[subindex];
				if(st.selected) {
					reqBody.push({
						"standard": st.standard,
						"sectionCount": st.sectionCount
					})
				}
			}
		}

		var button = event.target
		var originalTargetHtml = button.innerHTML
		Utils.markBusy(button)

		if(reqBody.length == 0) {
			this.appNotificationService.notify("You must select atleast 1 class","danger")
			Utils.markActive(button, originalTargetHtml)
		} else {
			this.appNetworkService.saveClassDetail(reqBody, this.school.id)
			.then(d => {
				Utils.markActive(button, originalTargetHtml)
				this.onUpdate.emit(this.school)
			})
			.catch(e => {
				if(e.status >= 500) {
					this.appNotificationService.notifyGenericError()
				} else if(e.status == 400 ) {
					this.appNotificationService.notify("Please fill in all required fields correctly.","danger")
				} else if(e.status == 401){
					this.appNetworkService.deleteAllCookies()
					window.location.reload();
				}
				Utils.markActive(button, originalTargetHtml)
			})
		}
	}

	ngOnInit() {}

}
