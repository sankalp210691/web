import { Component, Input, Inject } from '@angular/core';
import { AppNetworkService } from '../../services/app-network.service';
import { AppNotificationService } from '../../services/app-notification.service';
import { User } from '../../models/User'
import { Utils } from '../../Utils'
import { Router } from '@angular/router';
import { trigger, style, animate, transition } from '@angular/animations';
import { LoginComponent } from '../login/login.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';

@Component({
  selector: 'app-offlineApplyToSchool',
  templateUrl: './offlineApplyToSchool.component.html',
  styleUrls: ['./offlineApplyToSchool.component.css']
})
export class OfflineApplyToSchoolComponent {

  @Input() schoolId: number;
  @Input() schoolName:String;

  parentName: string;
  parentEmail: string;
  parentPhone: string;

  constructor(public appNetworkService: AppNetworkService,
    public appNotificationService: AppNotificationService,
    public dialogRef: MatDialogRef<OfflineApplyToSchoolComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Object,
    public dialog: MatDialog) {

    this.schoolId = data["schoolId"]
    this.schoolName = data["schoolName"]
  }

  close(event) {
    this.dialogRef.close()
  }

  showAuthModal() {
    let dialogRef = this.dialog.open(LoginComponent, {
      width: "30vw"
    });
  }

  applyToSchool(event) {

    if(!this.parentName) {
      this.appNotificationService.notify("Parent name must be provided", "danger")
      return
    }

    if(!this.parentEmail || !Utils.validateEmail(this.parentEmail)) {
      this.appNotificationService.notify("Parent email must be provided", "danger")
      return
    }

    if(this.parentPhone && !Utils.validatePhone(this.parentPhone)) {
      this.appNotificationService.notify("Invalid phone number", "danger")
      return
    }

    var button = event.target
    var originalTargetHtml = button.innerHTML
    Utils.markBusy(button)

    this.appNetworkService.offlineApplyToSchool(this.parentName, this.parentEmail, this.parentPhone, this.schoolId)
    .then(d => {
      this.appNotificationService.notify("Your interest has been successfully submitted", "info")
      Utils.markActive(button, originalTargetHtml)
      this.dialogRef.close()
    }).catch(error => {
        this.appNotificationService.notify("Your interest could not be submitted. Please try again later", "danger")
        Utils.markActive(button, originalTargetHtml)
      })
  }
}
