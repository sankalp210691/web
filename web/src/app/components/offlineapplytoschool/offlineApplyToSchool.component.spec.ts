import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfflineApplyToSchoolComponent } from './offlineApplyToSchool.component';

describe('OfflineApplyToSchoolComponent', () => {
  let component: OfflineApplyToSchoolComponent;
  let fixture: ComponentFixture<OfflineApplyToSchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfflineApplyToSchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfflineApplyToSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
