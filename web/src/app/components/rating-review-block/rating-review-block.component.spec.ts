import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingReviewBlockComponent } from './rating-review-block.component';

describe('RatingReviewBlockComponent', () => {
  let component: RatingReviewBlockComponent;
  let fixture: ComponentFixture<RatingReviewBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingReviewBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingReviewBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
