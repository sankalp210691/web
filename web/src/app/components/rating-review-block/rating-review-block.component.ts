import { Component, OnInit, Inject, Input, EventEmitter, Output } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { RatingReviewComponent } from '../rating-review/rating-review.component';

@Component({
	selector: 'app-rating-review-block',
	templateUrl: './rating-review-block.component.html',
	styleUrls: ['./rating-review-block.component.css']
})
export class RatingReviewBlockComponent implements OnInit {

	@Input() review;
	@Input() school;

	constructor(public dialog: MatDialog) { }

	ngOnInit() {
	}

	showReviewModal() {
		let dialogRef = this.dialog.open(RatingReviewComponent, {
			width: "40vw",
			maxHeight: "600px",
			data: { 
				reviewId: this.review["id"],
				schoolId: this.school["id"],
				schoolName: this.school["name"],
				rating: this.review["rating"],
				review: this.review["review"],
				associationType: this.review["associationType"]
			}
		});

		dialogRef.afterClosed().subscribe(ratingReview => {
			if(ratingReview) {
				this.review.rating = ratingReview["rating"]
				this.review.review = ratingReview["review"]
				this.review.associationType = ratingReview["associationType"]
			}
		});
	}

}
