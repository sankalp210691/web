import { Component, OnInit, Inject, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Utils } from '../../Utils';
import { SearchFilterPipe } from '../../pipes/SearchFilterPipe';
import { School } from '../../models/School';
import { Locality } from '../../models/Locality';
import { AppNetworkService } from '../../services/app-network.service';

export enum Theme {
	DEFAULT,
	WHITE
}

@Component({
	selector: 'app-school-search',
	templateUrl: './schoolsearch.component.html',	
	styleUrls: ['./schoolsearch.component.css']
})
export class SchoolSearchComponent implements OnInit {

	@Input() theme: Theme
	@Input() set filterQueryPath(value: string) {
		this._filterQueryPath = JSON.parse(value)
		if(this._filterQueryPath['q']) {
			this.queryFilter = this._filterQueryPath['q']
		}
		this.searchSchool(null)
	}
	public static ThemeType = Theme
	private appNetworkService: AppNetworkService;
	_filterQueryPath: Object = {st: 'prs,dc,ps,ms,ss,sss', sb: 'rt'};

	locationNameFilter = ""
	queryFilter = ""
	localityList = []
	queryList = {}
	selectedLocation = null
	selectedSchool = null
	showQueryListSuggestions = false;
	minQueryLength = 3;
	lnf=false
	showSuggestionsView = false;

	suggestedSearches = [
		{
			"name": "Daycare",
			"iconClass": "fa fa-search",
			"st": "dc"
		},
		{
			"name": "Preschool",
			"iconClass": "fa fa-search",
			"st": "prs"
		},
		{
			"name": "School",
			"iconClass": "fa fa-search",
			"st": "prs,ps,ms,ss,sss"
		},
		{
			"name": "Primary School",
			"iconClass": "fa fa-search",
			"st": "ps"
		},
		{
			"name": "Middle School",
			"iconClass": "fa fa-search",
			"st": "ms"
		},
		{
			"name": "Secondary School",
			"iconClass": "fa fa-search",
			"st": "ss"
		},
		{
			"name": "Senior Secondary School",
			"iconClass": "fa fa-search",
			"st": "sss"
		}
	];

	constructor(public searchfilter: SearchFilterPipe, private appns: AppNetworkService, private router: Router) { 

		this.appNetworkService = appns;
		this.fetchCities()
	}

	fetchCities() {
		if(this.localityList.length == 0) {
			this.appNetworkService.getLocationData(city => {
				if(city) {
					this.localityList = JSON.parse(JSON.stringify(city.localityList))
					var ct = city
					ct["isCity"] = true
					this.localityList.push(ct)
					this.setupLocationFromFilters()
				}
			})
		}
	}

	hideSuggestionsView() {
		setTimeout(function(){ 
			this.showSuggestionsView = false;
		}, 100);
	}

	setupLocationFromFilters() {
		if(this.localityList.length == 0) { return }
			var locId = this._filterQueryPath['lid'] ? this._filterQueryPath['lid'] : this._filterQueryPath['cid']//cityid or localityid. if nothing then hardcode to 1 = bangalore
		var locType = this._filterQueryPath['lid'] ? "lid" : "cid"
		if(!locId) {
			locId = 1
		}
		for(var index in this.localityList) {
			var loc = this.localityList[index]
			if(loc["id"] == locId && ((locType == "lid" && !loc["isCity"]) || (locType == "cid" && loc["isCity"]))) {
				this.setLocation(loc)
			}
		}
	}

	setLocation(locality) {
		this.selectedLocation = locality
		this.locationNameFilter = locality["name"]
		this.lnf = false
	}

	//for autocomplete
	searchQuery(event) {
		
		if(!this.queryFilter || this.queryFilter.length < this.minQueryLength || !this.selectedLocation) {
			this.showQueryListSuggestions = false;
			return
		}

		let key = this.queryFilter.substr(0, this.minQueryLength)
		var selectedLocationId = this.selectedLocation["id"]
		if(!this.queryList[selectedLocationId]) {
			this.queryList[selectedLocationId] = {}
		}
		if(!this.queryList[selectedLocationId][key] || !this.queryList[selectedLocationId][key]["list"]) {
			if(!this.queryList[selectedLocationId][key]) {
				this.queryList[selectedLocationId][key] = {"list": null, "reqInProg": false}
			}
			if(!this.queryList[selectedLocationId][key]["reqInProg"]) {
				this.queryList[selectedLocationId][key]["reqInProg"] = true
				this.appNetworkService.schoolSearchAutocomplete(key, selectedLocationId, this.selectedLocation["isCity"])
				.then(result => {
					if(result.status == 204) {
						this.queryList[selectedLocationId][key]["list"] = []
					} else {
						this.queryList[selectedLocationId][key]["list"] = result.json()
					}
					this.showQueryListSuggestions = true;
					this.queryList[selectedLocationId][key]["reqInProg"] = false
				}).catch(error => {
					this.queryList[selectedLocationId][key]["reqInProg"] = false
					this.showQueryListSuggestions = false;
				})
			}
		} else {
			this.showQueryListSuggestions = true;
		}
	}

	//for actual search
	searchSchool(suggestedSearch) {
		if(this.selectedLocation == null) {
			return;
		}
		if(this.selectedLocation["isCity"]) {
			this._filterQueryPath['cid'] = this.selectedLocation["id"]
			delete this._filterQueryPath['lid']
		} else {
			this._filterQueryPath['lid'] = this.selectedLocation["id"]
			delete this._filterQueryPath['cid']
		}
		if(!this._filterQueryPath['pgc']) {
			this._filterQueryPath['pgc'] = 1
		}
		if(suggestedSearch) {
			this._filterQueryPath['st'] = suggestedSearch['st']
		}
		if(!suggestedSearch) {
			if(this.queryFilter && this.queryFilter.trim().length > 0) {
				this._filterQueryPath['q'] = this.queryFilter.trim()
			} else {
				delete this._filterQueryPath['q']
			}
		}
		this.router.navigate(['/schools', this._filterQueryPath]);
	}

	getSearchButtonThemeColor(): string {
		switch(this.theme) {
			case Theme.DEFAULT: return "accent"
			case Theme.WHITE: return ""
			default: return "accent"
		}
	}

	ngOnInit(){
		if(!this.theme) {
			this.theme = Theme.DEFAULT
		}
		if(!this._filterQueryPath) {
			//default value
			this._filterQueryPath = {st: 'prs,dc,ps,ms,ss,sss', sb: 'rt'}
		}
		this.setupLocationFromFilters()
	}
}
