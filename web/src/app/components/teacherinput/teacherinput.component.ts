import { Component, OnInit, Directive, Input, Output, EventEmitter } from '@angular/core';
import { UploaderComponent } from '../uploader/uploader.component'
import { TeacherInputFormComponent } from '../teacherinputform/teacherinputform.component'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { School } from '../../models/School';
import { Teacher } from '../../models/Teacher';
import { SearchFilterPipe } from '../../pipes/SearchFilterPipe';
import { DatePipe } from '@angular/common';
import { Utils } from '../../Utils'
import { AppNotificationService } from '../../services/app-notification.service';
import { AppNetworkService } from '../../services/app-network.service';

@Component({
	selector: 'app-teacher-input',
	templateUrl: './teacherinput.component.html',
	styleUrls: ['./teacherinput.component.css']
})
export class TeacherInputComponent implements OnInit {

	@Input() school:School;
	@Input() teachers: Teacher[]
	@Output('onUpdate') onUpdate: EventEmitter<Object> = new EventEmitter<Object>();
	public file: File

	public teacherInputFormOpen: Boolean = false
	public teacherDataLoading: Boolean = false

	private appNotificationService: AppNotificationService;
	private appNetworkService: AppNetworkService;

	constructor(public dialog: MatDialog, public searchfilter: SearchFilterPipe, public datePipe: DatePipe,
		private ans: AppNotificationService,
		private appns: AppNetworkService) { 

		this.appNotificationService = ans;
		this.appNetworkService = appns;

	}

	inputTypeChange(type) {
		switch(type) {
			case "keyin": this.onUpdate.emit({'teachers': this.teachers, 'file': null})
			break
			case "csv": this.onUpdate.emit({'teachers': null, 'file': this.file})
			break
		}
	}

	ngOnInit() {
		this.teacherDataLoading = true
		this.appNetworkService.getTeachersBySchool()
		.then(d => {
			this.teachers = d.json()
			if(this.teachers) {
				for( var index in this.teachers) {
					var teacher = this.teachers[index]
					teacher.name = teacher.user.name
					for(var index in this.school.classList) {
						let clazz = this.school.classList[index];
						if(clazz["id"] == teacher.ctClass) {
							teacher.ctClass = clazz
						}
					}
				}
			}
			this.teacherDataLoading = false
			this.onUpdate.emit({'teachers': this.teachers, 'file': null})
		})
		.catch(e => {
			this.teacherDataLoading = false
			if(e.status >= 500) {
				this.appNotificationService.notifyGenericError()
			}else if(e.status == 400 ) {
				var error = JSON.parse(e._body).error
				this.appNotificationService.notify(error,"danger")
			} else if(e.status == 401){
				this.appNotificationService.notify("You are not authorized","danger")
			}
		})
	}

	onFileUpload(event) {
		this.file = event
		this.onUpdate.emit({'teachers': null, 'file': this.file})
	}

	deleteTeacher(event, teacherIndex) {
		var self = this;
		this.appNotificationService.confirm("Delete Teacher", "Are you sure you want to delete this teacher?", function (event) {
			if(!self.teachers) {
				return
			}
			if(teacherIndex < self.teachers.length) {
				var deleteButton = event.target
				var buttonContent = deleteButton.innerHTML
				Utils.markBusy(deleteButton)

				self.appNetworkService.deleteTeacher(self.teachers[teacherIndex].id)
				.then(d=>{
					self.teachers.splice(teacherIndex, 1)
					this.appNotificationService.notify("Teacher deleted successfully", "info")
					self.onUpdate.emit({'teachers': self.teachers, 'file': null})
				})
				.catch(e => {
					if(e.status >= 500) {
						this.appNotificationService.notifyGenericError()
					} else if(e.status == 401){
						this.appNotificationService.notify("You are not authorized to perform this action", "danger")
					}
					Utils.markActive(deleteButton, buttonContent)
				})
			}
		})
	}

	showTeacherForm(event, teacherIndex) {
		if(!this.teachers) {
			this.teachers = []
		}
		let dialogRef = this.dialog.open(TeacherInputFormComponent, {
			width: "40vw",
			disableClose:true,
			data: { teacher: teacherIndex != null ? this.teachers[teacherIndex] : null, school: this.school }
		});

		dialogRef.afterClosed().subscribe(teacher => {
			if(teacher) {
				teacher.name = teacher.user.name
				for(var index in this.school.classList) {
					let clazz = this.school.classList[index];
					if(clazz["id"] == teacher.ctClass) {
						teacher.ctClass = clazz
					}
				}
				if(teacher && (!teacherIndex && teacherIndex != 0)) {
					this.teachers.push(teacher)
				} else if (teacher) {
					this.teachers[teacherIndex] = teacher
				}
			}
			this.onUpdate.emit({'teachers': this.teachers, 'file': null})
		});
	}

}
