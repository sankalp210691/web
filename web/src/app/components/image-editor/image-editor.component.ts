import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';

@Component({
	selector: 'app-image-editor',
	templateUrl: './image-editor.component.html',
	styleUrls: ['./image-editor.component.css']
})
export class ImageEditorComponent implements OnInit {

	@Output('onDone') onDone: EventEmitter<File> = new EventEmitter<File>();

	originalFileEvent:any = ''
	croppedImage:any
	aspectRatio

	constructor(public dialogRef: MatDialogRef<ImageEditorComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any) {

		this.originalFileEvent = data.file;
		this.aspectRatio = data.aspectRatio;
	}

	ngOnInit() { }

	imageCropped(event: string) {
		this.croppedImage = event
	}

	imageLoaded() {
		console.log("showing cropper")
	}
	loadImageFailed() {
		console.log("image loading failed")
	}

	save(event) {
		this.dialogRef.close(this.croppedImage);
	}

	cancel(event) {
		this.dialogRef.close();
	}
}
