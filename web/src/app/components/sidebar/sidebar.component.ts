import { Component, OnInit, Inject, Input, EventEmitter, Output } from '@angular/core';
import { Utils } from '../../Utils';
import { AppNotificationService } from '../../services/app-notification.service';
import { AppNetworkService } from '../../services/app-network.service';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

	isLoggedIn: boolean = false

	constructor(private appNetworkService: AppNetworkService, private router: Router, public dialog: MatDialog) { 
		
	}
}
