import { Component, OnInit, Inject, Input, EventEmitter, Output } from '@angular/core';
import { Utils } from '../../Utils';

enum RatingType {
	ZERO,
	FULL,
	PARTIAL
}

@Component({
	selector: 'app-rating',
	templateUrl: './rating.component.html',	
	styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {

	@Input() rating:number;
	@Input() allowEditing:boolean;
	@Input() mini:boolean;
	@Output('onUpdate') onUpdate: EventEmitter<number> = new EventEmitter<number>();

	maxRating = 5
	ratingRange = []
	ratingTypeRef = RatingType

	constructor() {}

	ngOnInit() {
		if(!this.rating) {
			this.rating = 0
		}
		this.setRating(this.rating)
	}

	setPermRating(rating) {
		if(this.allowEditing) {
			if(rating == this.rating) {
				this.rating = rating - 1
			} else {
				this.rating = rating
			}
			this.setRating(this.rating)
		}
	}

	setRating(rating) {
		for(var index = 1; index <= this.maxRating; index++) {
			if(index <= rating) {
				this.ratingRange[index-1] = RatingType.FULL
			} else if(index - rating > 0 && index - rating < 1) {
				this.ratingRange[index-1] = RatingType.PARTIAL
			} else {
				this.ratingRange[index-1] = RatingType.ZERO
			}
		}
		this.onUpdate.emit(this.rating)
	}
}
