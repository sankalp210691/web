import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectinputformComponent } from './subjectinputform.component';

describe('SubjectinputformComponent', () => {
  let component: SubjectinputformComponent;
  let fixture: ComponentFixture<SubjectinputformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectinputformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectinputformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
