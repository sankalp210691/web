import { Component, OnInit, Directive, Input, Output, EventEmitter, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { School } from '../../models/School'
import { Class } from '../../models/Class'
import { Teacher } from '../../models/Teacher'
import { Subject } from '../../models/Subject'
import { Utils } from '../../Utils'
import { AppNotificationService } from '../../services/app-notification.service';
import { AppNetworkService } from '../../services/app-network.service';

@Component({
  selector: 'app-subjectinputform',
  templateUrl: './subjectinputform.component.html',
  styleUrls: ['./subjectinputform.component.scss']
})
export class SubjectinputformComponent implements OnInit {

	@Input() subject: Subject
	@Output('onDone') onDone: EventEmitter<Subject> = new EventEmitter<Subject>();
	public school: School
	public teachers: Teacher[]

  constructor(public dialogRef: MatDialogRef<SubjectinputformComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public appNotificationService: AppNotificationService,
    public appNetworkService: AppNetworkService) { 

    this.school = data.school;
    this.teachers = data.teachers;
    for(var index in this.teachers) {
      let teacher = this.teachers[index]
      teacher["name"] = teacher.user.name
    }
    if(data.subject) {
      this.subject = data.subject
    } else {
      this.subject = new Subject()
    }
  }

  ngOnInit() {
  }

  cancel(event) {
    this.dialogRef.close();
  }

  save(event) {
    var button = event.target
    var buttonContent = button.innerHTML
    Utils.markBusy(button)

    if(!this.subject.name) {
      this.appNotificationService.notify("You must provide a subject name", "danger")
      Utils.markBusy(button)
      return
    }
  }

}
