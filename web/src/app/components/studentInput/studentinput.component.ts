import { Component, OnInit, Directive, Input, Output, EventEmitter } from '@angular/core';
import { UploaderComponent } from '../uploader/uploader.component'
import { StudentInputFormComponent } from '../studentinputform/studentinputform.component'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { School } from '../../models/School';
import { Student } from '../../models/Student';
import { SearchFilterPipe } from '../../pipes/SearchFilterPipe';
import { DatePipe } from '@angular/common';
import { Utils } from '../../Utils'
import { AppNotificationService } from '../../services/app-notification.service';
import { AppNetworkService } from '../../services/app-network.service';

@Component({
	selector: 'app-student-input',
	templateUrl: './studentinput.component.html',
	styleUrls: ['./studentinput.component.css']
})
export class StudentInputComponent implements OnInit {

	@Input() school:School;
	@Input() students: Student[]
	@Output('onUpdate') onUpdate: EventEmitter<Object> = new EventEmitter<Object>();
	public file: File

	public studentInputFormOpen: Boolean = false
	public studentDataLoading: Boolean = false

	private appNotificationService: AppNotificationService;
	private appNetworkService: AppNetworkService;

	constructor(public dialog: MatDialog, public searchfilter: SearchFilterPipe, public datePipe: DatePipe,
		private ans: AppNotificationService,
		private appns: AppNetworkService) { 

		this.appNotificationService = ans;
		this.appNetworkService = appns;

	}

	inputTypeChange(type) {
		switch(type) {
			case "keyin": this.onUpdate.emit({'students': this.students, 'file': null})
			break
			case "csv": this.onUpdate.emit({'students': null, 'file': this.file})
			break
		}
	}

	ngOnInit() {
		this.studentDataLoading = true
		this.appNetworkService.getStudentsBySchool()
		.then(d => {
			this.students = d.json()
			this.studentDataLoading = false
			this.onUpdate.emit({'students': this.students, 'file': null})
		})
		.catch(e => {
			this.studentDataLoading = false
			if(e.status >= 500) {
	          this.appNotificationService.notifyGenericError()
	        }else if(e.status == 400 ) {
	          var error = JSON.parse(e._body).error
	          this.appNotificationService.notify(error,"danger")
	        } else if(e.status == 401){
	          this.appNotificationService.notify("You are not authorized","danger")
	        }
		})
	}

	onFileUpload(event) {
		this.file = event
		this.onUpdate.emit({'students': null, 'file': this.file})
	}

	deleteStudent(event, studentIndex) {
		var self = this;
		this.appNotificationService.confirm("Delete Student", "Are you sure you want to delete this student?", function (event) {
			if(!self.students) {
				return
			}
			if(studentIndex < self.students.length) {
				var deleteButton = event.target
				var buttonContent = deleteButton.innerHTML
				Utils.markBusy(deleteButton)

				self.appNetworkService.deleteStudent(self.students[studentIndex].id)
				.then(d=>{
					self.students.splice(studentIndex, 1)
					self.onUpdate.emit({'students': self.students, 'file': null})
					this.appNotificationService.notify("Student deleted successfully", "info")
				})
				.catch(e => {
					if(e.status >= 500) {
				        this.appNotificationService.notifyGenericError()
				      } else if(e.status == 401){
				        this.appNotificationService.notify("You are not authorized to perform this action", "danger")
				      }
				      Utils.markActive(deleteButton, buttonContent)
				})
			}
		})
	}

	showStudentForm(event, studentIndex) {
		if(!this.students) {
			this.students = []
		}
		let dialogRef = this.dialog.open(StudentInputFormComponent, {
			width: "75vw",
			maxHeight: "75vh",
			disableClose:true,
			data: { student: studentIndex != null ? this.students[studentIndex] : null, school: this.school }
		});

		dialogRef.afterClosed().subscribe(student => {
			if(student && (!studentIndex && studentIndex != 0)) {
				this.students.push(student)
			} else if (student) {
				this.students[studentIndex] = student
			}
			this.onUpdate.emit({'students': this.students, 'file': null})
		});
	}

}
