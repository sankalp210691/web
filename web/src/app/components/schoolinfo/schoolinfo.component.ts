import { Component, OnInit, Directive, Input, Output, EventEmitter, Inject, Optional } from '@angular/core';
import { UploaderComponent } from '../uploader/uploader.component'
import { School } from '../../models/School';
import { Utils } from '../../Utils'
import { AppNotificationService } from '../../services/app-notification.service';
import { AppNetworkService } from '../../services/app-network.service';
import { Board } from '../../models/Board';
import { Country } from '../../models/Country';
import { State } from '../../models/State';
import { City } from '../../models/City';
import { Locality } from '../../models/Locality';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';

@Component({
	selector: 'app-school-info',
	templateUrl: './schoolinfo.component.html',
	styleUrls: ['./schoolinfo.component.css']
})
export class SchoolInfoComponent implements OnInit {

	@Input() school:School;
	@Output('onUpdate') onUpdate: EventEmitter<School> = new EventEmitter<School>();
	@Output('onCancel') onCancel: EventEmitter<any> = new EventEmitter<any>();

	private adminId: number;
	private openInDialog: boolean = false;
	private facilityColumnCount: number = 3;
	public isBoardRequired: boolean = false
	public showImageUploader: boolean = true
	public instituteCategories = [
	{"id": 1, "name": "Preschool"},
	{"id": 2, "name": "Daycare"},
	{"id": 3, "name": "Primary School"},
	{"id": 4, "name": "Middle School"},
	{"id": 5, "name": "Secondary School"},
	{"id": 6, "name": "Senior Secondary School"},
	{"id": 7, "name": "College"},
	{"id": 8, "name": "University"},
	{"id": 9, "name": "Coaching Institute"},
	{"id": 10, "name": "Special Skills Institutes"}
	];
	public boardList: Board[];
	public boardListIsLoading: Boolean = true;
	public selectedBoard: Board;

	public countryList: Country[];
	public countryListIsLoading: Boolean = true;
	public selectedCountry: Country;

	// public stateList: State[];
	// public stateListIsLoading: Boolean = true;
	public selectedState: State;

  	// public cityList: City[];
  	// public cityListIsLoading: Boolean = true;
  	public selectedCity: City;

  	schoolFacilities = [[]];

  	public localityListIsLoading: Boolean = true;

  	constructor(public appNotificationService: AppNotificationService,  public appNetworkService: AppNetworkService,
  	 @Optional() @Inject(MAT_DIALOG_DATA) public dialogData: any,
  	 @Optional() public dialogRef: MatDialogRef<SchoolInfoComponent>) {
  		if(dialogData) {
  			this.school = dialogData.school;
  			this.showImageUploader = false;
  			this.openInDialog = true;
  		} else {
  			this.school["facilities"] = []
  		}
  		
  		this.appNetworkService.getBoardList()
  		.then(d => {
  			let data = d.json();
  			this.boardList = data.list;
  			this.boardListIsLoading = false;
  			if(this.school["boardId"]) {
  				for(var index in this.boardList) {
  					let board = this.boardList[index]
  					if(board["id"] == this.school["boardId"]) {
  						this.school["board"] = board
  						break
  					}
  				}
  			}
  		}).catch(e => {
  			this.boardListIsLoading = false;
  		});

	    //UNCOMMENT TO GET STATE LIST
	    // this.appNetworkService.getStateList()
	    // .then(d => {
	    //   let data = d.json();
	    //   console.log(data)
	    //   this.stateList = data.list;
	    //   this.stateListIsLoading = false;
	    // }).catch(e => {
	    //   this.stateListIsLoading = false;
	    // });

	    // this.appNetworkService.getCityList(this.selectedState.id)
	    // .then(d => {
	    //   let data = d.json();
	    //   console.log(data)
	    //   this.cityList = data.list;
	    //   this.cityListIsLoading = false;
	    // }).catch(e => {
	    //   this.cityListIsLoading = false;
	    // });

	    let schoolFacilitiesArray = Utils.schoolFacilities;
	    var row = 0, col = 0;
	    this.schoolFacilities[row] = []
	    for(var index in schoolFacilitiesArray) {
	    	this.schoolFacilities[row][col] = schoolFacilitiesArray[index];
	    	col++
	    	if(col == this.facilityColumnCount) {
	    		col = 0
	    		row++
	    		this.schoolFacilities[row] = []
	    	}
	    }

	    this.selectedCountry = { id: 1, name: "India", code: "IN", internationalPhoneCode: "91", stateList: [] }  //hardcoded india
	    this.selectedState = { id: 17, name: "Karnataka", country: this.selectedCountry, cityList: [] } //hardcoded karnataka
	    this.selectedCountry.stateList.push(this.selectedState)
	    this.selectedCity = { id : 1, name: "Bengaluru", code: "Bengaluru", state: this.selectedState, localityList: [] } //hardcoded bengaluru
	    this.selectedState.cityList.push(this.selectedCity)
	    this.appNetworkService.getLocalityList(this.selectedCity)
	    .then(d => {
	    	let data = d;
	    	this.selectedCity.localityList = data;
	    	this.localityListIsLoading = false;
	    	if(dialogData) {
	    		for(var index in this.selectedCity.localityList) {
	    			var locality = this.selectedCity.localityList[index]
	    			if(locality["id"] == this.school.locality.id) {
	    				this.school["locality"] = locality
	    				break;
	    			}
	    		}
	    	}
	    }).catch(e => {
	    	this.localityListIsLoading = false;
	    });
	}
	ngOnInit() {}

	onUpdateInstituteCategory(event) {
		this.school.instituteCategoryList = event
		this.isBoardRequired = false
		for(var index in this.school.instituteCategoryList) {
			var tag = this.school.instituteCategoryList[index]
			if(tag.id >= 3 && tag.id <= 6) {
				this.isBoardRequired = true
			}
		}
	}

	onUpdateBoard(event) {
		this.school.boardList = event
	}

	onSchoolImageUpload(event) {
		this.school.displayPicImage = event
	}

	cancel(event) {
		this.onCancel.emit(event)
		if(this.openInDialog) {
			this.dialogRef.close(null);
		}
	}

	saveSchoolInfo(event) {
		var button = event.target
		var originalTargetHtml = button.innerHTML
		Utils.markBusy(button)
		var self = this;
		this.saveSchoolInfoNetworkCall(event.target,success =>  {
			if(success){
				self.onUpdate.emit(this.school)
				if(self.openInDialog) {
					self.dialogRef.close(this.school);
				}
			}
			Utils.markActive(button, originalTargetHtml)
		})
	}

	facilitySelection(event, facility) {
		if(event.checked) {
			this.school['facilities'].push(facility['code'])
		} else {
			this.school['facilities'].splice(this.school['facilities'].indexOf(facility['code']), 1)
		}
	}

	private saveSchoolInfoNetworkCall(target, callback) {
		if(!this.school.name || !this.school.address || !this.school.locality || !this.school.pincode || !this.school.instituteCategoryList 
			|| this.school.instituteCategoryList.length == 0 || !this.school || (this.school.instituteCategoryList.forEach(ic => {var found = false; if(ic.id >= 3  && ic.id <= 6) {found = true} return found}) && !this.school.boardList)) {

			this.appNotificationService.notify("One or more fields are missing", "danger")
			callback(false);
			return;
		}

		if(this.school.email && this.school.email.length > 200) {
			this.appNotificationService.notify("Email cannot be more than 200 characters in length", "danger")
			callback(false);
			return;
		}

		if(this.school.phone && this.school.phone.length > 200) {
			this.appNotificationService.notify("Phone number cannot be more than 200 characters in length", "danger")
			callback(false);
			return;
		}

		if(this.school.displayPicImage && !Utils.checkFileExtention(this.school.displayPicImage.name, "jpg") && !Utils.checkFileExtention(this.school.displayPicImage.name, "jpeg") && !Utils.checkFileExtention(this.school.displayPicImage.name, "png")) {
			this.appNotificationService.notify("Please upload a jpg, jpeg or png image file", "danger")
			callback(false);
			return;
		}

		this.appNetworkService.saveSchoolDetail(this.school)
		.then(d=> {
			var data = d.json();
			if(data) {
				this.school.id = data.map.schoolId
				this.adminId = data.map.adminId
			}
			callback(true);
		})
		.catch(e => {
			if(e.status >= 500) {
				this.appNotificationService.notifyGenericError()
			}else if(e.status == 400 ) {
				this.appNotificationService.notify("Please fill in all required fields correctly.","danger")
			} else if(e.status == 401){
				this.appNetworkService.deleteAllCookies()
				window.location.reload();
			}
			callback(false)
		});
	}

}
