import { Component, OnInit } from '@angular/core';
import { AppNetworkService } from '../../services/app-network.service';
import { AppNotificationService } from '../../services/app-notification.service';
import { User } from '../../models/User'
import { Utils } from '../../Utils'
import { Router } from '@angular/router';
import { trigger, style, animate, transition, query, stagger } from '@angular/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit{

  oldPassword:string
  newPassword:string
  repeatNewPassword:string

  phone:string;
  email:string;

  constructor(public appNetworkService: AppNetworkService, public appNotificationService: AppNotificationService, public router: Router, public dialogRef: MatDialogRef<SettingsComponent>) { 
  }

  ngOnInit() {
    let user = this.appNetworkService.getUser()
    this.phone = user.phone
    this.email = user.email
  }

  updatePassword(event) {
    var button = event.target
    var buttonContent = button.innerHTML

    if(Utils.validatePassword(this.oldPassword) && Utils.validatePassword(this.newPassword) && Utils.validatePassword(this.repeatNewPassword) && this.newPassword == this.repeatNewPassword) {
      Utils.markBusy(button)
      this.appNetworkService.updatePassword(this.oldPassword, this.newPassword, this.repeatNewPassword)
      .then(d => {
        this.appNotificationService.notify("Password updated successfully", "info")
        Utils.markActive(button, buttonContent)
      }).catch(error => {
        if(error.status >= 500) {
          this.appNotificationService.notify("We encountered an issue while updating password. Please try again later.", "danger")
        } else {
          this.appNotificationService.notify("Old password is incorrect", "danger")
        }
        Utils.markActive(button, buttonContent)
      })
    } else {

      if(!Utils.validatePassword(this.oldPassword)) {
        this.appNotificationService.notify("Invalid old password. Passwords must be atleast 6 characters long.", "danger")
      }
      if(!Utils.validatePassword(this.newPassword)) {
        this.appNotificationService.notify("Invalid new password. Passwords must be atleast 6 characters long.", "danger")
      }
      if(this.newPassword != this.repeatNewPassword) {
        this.appNotificationService.notify("New Password and Repeated new password do not match", "danger")
      }
    }
  }

  saveContactDetails(event) {
    var button = event.target
    var buttonContent = button.innerHTML

    if(Utils.validatePhone(this.phone) && Utils.validateEmail(this.email)) {
      Utils.markBusy(button)
      //HANDLE NETWORK CALL
    } else {
      if(!Utils.validatePhone(this.phone)) {
        this.appNotificationService.notify("Invalid phone number. Please enter valid phone number.", "danger")
      }
      if(!Utils.validateEmail(this.email)) {
        this.appNotificationService.notify("Invalid email. Please enter valid email address.", "danger")
      }
    }
  }
  
}
