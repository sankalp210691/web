export class Utils {

  private static oldDate = new Date('June 21, 1991 16:30:00');
  private static futureDate = new Date('June 21, 3000 16:30:00');

  private static genders = [
  {"value": "M", "text": "Male"},
  {"value": "F", "text": "Female"},
  {"value": "O", "text": "Other"},
  ]

  private static supportedFileInfo = {
    "csv": { "normalizedType" : "file", "type": "csv"},
    "docx": { "normalizedType" : "file", "type": "docx"},
    "doc": { "normalizedType" : "file", "type": "doc"},
    "jpg": { "normalizedType" : "image", "type": "image/jpg"},
    "jpeg": { "normalizedType" : "image", "type": "image/jpeg"},
    "png": { "normalizedType" : "image", "type": "image/png"}
  }

  public static schoolFacilities = [
    {
      "code": "ac",
      "name": "Air Conditioner",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/air-conditioner.png"
    },
    {
      "code": "amp",
      "name": "Amphitheater",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/theater.png"
    },
    {
      "code": "cnt",
      "name": "Canteen",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/plate.png"
    },
    {
      "code": "hstl",
      "name": "Hostel",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/hostel.png"
    },
    {
      "code": "lab",
      "name": "Laboratory",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/experiment.png"
    },
    {
      "code": "lib",
      "name": "Library",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/bookshelf.png"
    },
    {
      "code": "med",
      "name": "First Aid",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/medical-kit.png"
    },
    {
      "code": "plgr",
      "name": "Play Ground",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/soccer-field.png"
    },
    {
      "code": "sec",
      "name": "Security",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/cctv.png"
    },
    {
      "code": "trns",
      "name": "Transport",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/school-bus.png"
    },
    {
      "code": "spool",
      "name": "Swimming Pool",
      "icon": "https://s3.us-east-2.amazonaws.com/mgkslt-prod-public/images/swim.png"
    }
  ]

  public static markBusy(button) {
    button.disabled = true
    button.innerHTML = "<img src='/assets/img/loading.gif' style='width:20px;height:20px'>";
  }

  public static markActive(button, originalButtonContent) {
    button.disabled = false
    button.innerHTML = originalButtonContent;
  }

  public static checkFileExtention(filename, accepts): boolean {
    var extension = filename.split('.').pop();
    return extension.toLowerCase() == accepts
  }

  public static getDisplayUpdatedAt(timestamp): string {
    var currentTimestamp = new Date().getTime()
    var diff = (currentTimestamp - timestamp)/1000
    if(diff < 1){
      return "just now"
    }else if(diff < 15) {
      return Math.floor(diff) + " seconds ago"
    } else if (diff < 60) {
      return " few seconds ago"
    } else if (diff < 90) {
      return " about a minute ago"
    } else if (diff < 3600) {
      return Math.floor(diff/60) + " minutes ago"
    } else if(diff < 3600 * 24) {
      return Math.floor(diff/3600) + " hours ago"
    } else if(diff < 3600 * 48) {
      return "yesterday"
    } else {
      var localDateTime = (new Date(timestamp)).toLocaleDateString()
      return "on " + localDateTime
    }
  }

  public static validatePhone(phone: string): Boolean {
    if(!phone || phone.trim().match(/^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/) == null) {
      return false;
    }
    return true;
  }

  public static validatePassword(password: string): Boolean {
    if(!password || password.length < 6) {
      return false;
    }
    return true;
  }

  public static validateEmail(email: string): Boolean {
    if(!email || email.trim().match(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/) == null) {
      return false
    }
    return true
  }

  public static getGenders() {
    return this.genders
  }

  public static getSupportedFileInfo() {
    return this.supportedFileInfo
  }

  public static isValidImageFile(fileType) {
    var ftype = fileType.split("/")[1]
    if(ftype) {
      if(!this.supportedFileInfo[ftype]) {
        return false
      }
      return this.supportedFileInfo[ftype]["normalizedType"] == "image"
    }
    return false;
  }

  public static isEmptyObject(obj) {
    for(var key in obj) {
      if(obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }

  public static dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type:mime});
  }

  public static getOldDateString(): string {
    return Utils.oldDate.toISOString()
  }

  public static getFutureDateString(): string {
    return Utils.futureDate.toISOString()
  }

  public static getSchoolFacilityByCode(code: string): Object {
    for(var index in Utils.schoolFacilities) {
      var facility = Utils.schoolFacilities[index]
      if(facility["code"] == code) {
        return facility
      }
    }
    return null
  }
}