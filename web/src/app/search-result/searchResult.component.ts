import { Injectable } from '@angular/core';
import { Component, OnInit, Directive, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { School } from '../models/School';
import { SearchFilterPipe } from '../pipes/SearchFilterPipe';
import { Utils } from '../Utils'
import { AppNotificationService } from '../services/app-notification.service';
import { AppNetworkService } from '../services/app-network.service';
import { SchoolSearchComponent } from '../components/schoolsearch/schoolsearch.component';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { LoginComponent } from '../components/login/login.component';
import { OfflineApplyToSchoolComponent } from '../components/offlineapplytoschool/offlineApplyToSchool.component';

enum SearchSettingInputType {
	CHECKBOX,
	RADIO
}

@Component({
	selector: 'app-search-result',
	templateUrl: './searchResult.component.html',
	styleUrls: ['./searchResult.component.css']
})
export class SearchResultComponent implements OnInit {

	private activeRouteSubscription: any;
	private appNotificationService: AppNotificationService;
	private appNetworkService: AppNetworkService;
	
	private filters = {}
	private offset = 0
	private lastQueriedOffset = -1
	private schoolJson = {}

	filtersString: string = "{}"
	searchSettings = []
	schoolSearchThemeType
	SearchSettingInputTypeRef = SearchSettingInputType
	isBusyFetchingResults = true
	schoolList = []
	resultCount = 0
	socialButtons = []
	schoolSearchTitle = "Schools in Bengaluru"
	resultsPerPage = 20
	pageIndex = null

	constructor(public dialog: MatDialog, public searchfilter: SearchFilterPipe,
		private ans: AppNotificationService,
		private appns: AppNetworkService, private router: Router, private route: ActivatedRoute,
		private schoolTypeSetting: SchoolTypeSetting, private schoolBoardSetting: SchoolBoardSetting,
		private otherFiltersSetting: OtherFiltersSetting, private facilityFiltersSetting: FacilityFiltersSetting) {

		this.appNotificationService = ans;
		this.appNetworkService = appns;
		this.schoolSearchThemeType = SchoolSearchComponent.ThemeType

		this.searchSettings.push(schoolTypeSetting)
		this.searchSettings.push(schoolBoardSetting)
		this.searchSettings.push(facilityFiltersSetting)
		this.searchSettings.push(otherFiltersSetting)

	}

	ngOnInit() {
		this.activeRouteSubscription = this.route.params.subscribe(params => {
			this.offset = 0
			this.schoolList = []
			this.schoolJson = {}
			this.lastQueriedOffset = -1
			this.filters = {}
			for(var key in params) {
				if(params.hasOwnProperty(key)) {
					this.filters[key] = params[key]
				}
				if(key == "pgc") {
					this.offset = +params[key] - 1;
				}
			}
			for(var index in this.searchSettings) {
				var ss = this.searchSettings[index];
				ss.setupWithQueryParams(this.filters[ss.getQueryParamsKey()])
			}
			if(!params["pgc"] || +params["pgc"] < 1) {
				params["pgc"] = 1
			}
			if(this.pageIndex == null) {
				this.pageIndex = this.filters["pgc"] - 1
			}
			this.getSchoolsWithFilter()
			this.socialButtons = [
			{
				"href": "https://www.facebook.com/sharer.php?t=" + encodeURIComponent(this.schoolSearchTitle) + "&u=" + encodeURI(window.location.href),
				"style": { color: '#3b5998' },
				"fontawesomeClass": "fab fa-facebook-square"
			},
			{
				"href": "https://twitter.com/intent/tweet?text="+ encodeURIComponent(this.schoolSearchTitle) + "&url=" + encodeURI(window.location.href),
				"style": { color: '#1da1f2' },
				"fontawesomeClass": "fab fa-twitter-square"
			},
			{
				"href": "https://plus.google.com/share?url=" + encodeURI(window.location.href),
				"style": { color: '#d34836' },
				"fontawesomeClass": "fab fa-google-plus-square"
			},
			{
				"href": "https://www.facebook.com/dialog/send?link=" + encodeURI(window.location.href) + "&redirect_uri=" + encodeURI(window.location.href) + "&app_id=521270401588372",
				"style": { color: '#0084ff' },
				"fontawesomeClass": "fab fa-facebook-messenger"
			},
			{
				"href": "https://pinterest.com/pin/create/button/?description=" + encodeURIComponent(this.schoolSearchTitle) + "&url=" + encodeURI(window.location.href),
				"style": { color: '#bd081c' },
				"fontawesomeClass": "fab fa-pinterest-square"
			},
			{
				"href": "https://web.whatsapp.com/send?text=" + encodeURI(window.location.href),
				"style": { color: '#25d366' },
				"fontawesomeClass": "fab fa-whatsapp-square"
			},
			{
				"href": "https://www.linkedin.com/sharing/share-offsite?title=" + encodeURIComponent(this.schoolSearchTitle) + "&url=" + encodeURI(window.location.href),
				"style": { color: '#0077b5' },
				"fontawesomeClass": "fab fa-linkedin"
			}
			]
		});
	}

	getSchoolsWithFilter() {
		this.isBusyFetchingResults = true
		this.filtersString = JSON.stringify(this.filters)
		this.appNetworkService.getSchools(this.filters, this.offset)
		.then(data => {
			let user = this.appNetworkService.getUser()
			this.lastQueriedOffset = this.offset
			this.offset = +this.offset + 1
			data = data.json()
			if(!data) {
				this.isBusyFetchingResults = false
				this.schoolList = []
				this.resultCount = 1
				return;
			}
			if(data["schoolList"]) {
				this.schoolList = data["schoolList"]
				this.resultCount = data["tc"]
			}
			this.isBusyFetchingResults = false
			for(var index in this.schoolList) {
				var school = this.schoolList[index]
				if(user && user["interestedSchools"]) {
					if(user["interestedSchools"].indexOf(+school['sid']) > -1) {
						school["userAlreadyShownInterest"] = true;
					}
				}
				if(this.schoolJson[school["sid"]]) {
					continue
				}
				this.schoolJson[school["sid"]] = school
				school.displayPic = school.displayPic ? school.displayPic : "../assets/img/dummy-school-dp.jpg"
				school.instituteCategoryName = school.instituteCategoryName.split(",")
				school.InstituteCategoryId = school.InstituteCategoryId.split(",")
			}

			
		})
		.catch(error => {
			console.log(error)
			this.isBusyFetchingResults = false
		})
	}

	applyToSchool(event, schoolId, schoolName) {

		var button = event.target
		var originalTargetHtml = button.innerHTML
		Utils.markBusy(button)

		if(this.appNetworkService.verifyIfLoggedIn()) {
			this.appNetworkService.applyToSchool(schoolId)
			.then(d => {
				this.appNotificationService.notify("Your interest has been successfully submitted", "info")
				this.schoolJson[schoolId]["userAlreadyShownInterest"] = true
				Utils.markActive(button, originalTargetHtml)
			}).catch(error => {
				this.appNotificationService.notify("Your interest could not be submitted. Please try again later", "danger")
				Utils.markActive(button, originalTargetHtml)
			})
			//do error handeling and all
		} else {
			let dialogRef = this.dialog.open(OfflineApplyToSchoolComponent, {
				width: "40vw",
				data: { schoolId: schoolId, schoolName: schoolName }
			});

			dialogRef.afterClosed().subscribe(result => {
				Utils.markActive(button, originalTargetHtml)
			});
		}
	}

	applyFilter(setting: SearchResultSetting) {
		let filterQuery = setting.getFilterQuery()
		let filterQueryKey = setting.getQueryParamsKey()

		if(filterQuery) {
			this.filters[filterQueryKey] = filterQuery
		} else {
			delete this.filters[filterQueryKey]
		}
		this.filters['pgc'] = 1
		this.filtersString = JSON.stringify(this.filters)
	}

	updatePage(event) {
		this.filters["pgc"] = event.pageIndex + 1
		this.offset = event.pageIndex

		this.filtersString = JSON.stringify(this.filters)
	}

	ngOnDestroy() {
		this.activeRouteSubscription.unsubscribe();
	}
}

//MARK: SEARCH FILTER SUPPORT CLASSES
abstract class SearchResultSetting {
	abstract getSectionName(): string
	abstract getInputType(): SearchSettingInputType
	abstract getOptionList(): SearchSettingOption[]
	abstract showFilterButton(): boolean
	abstract getQueryParamsKey(): string
	abstract searchableFilterPlaceholder(): string
	abstract setQueryDictForRetrival(dict): void

	setupWithQueryParams(queryString: string) {
		let queryArray = []
		if(queryString) {
			queryArray = queryString.split(",")
		}
		let queryDict = {}
		for(var index in queryArray) {
			let filterQuery = queryArray[index]
			queryDict[filterQuery] = true
		}
		if(this.getOptionList() && this.getOptionList().length > 0) {
			for(var index in this.getOptionList()) {
				var option = this.getOptionList()[index]
				if(queryDict[option.getValue()]) {
					option.setSelected(true)
				} else {
					option.setSelected(false)
				}
			}
		} else {
			this.setQueryDictForRetrival(queryDict)
		}
	}

	getFilterQuery(): string {
		var filterQuery = ""
		for(var index in this.getOptionList()) {
			let option =  this.getOptionList()[index]
			if(option.isSelected()) {
				filterQuery += option.getValue() + ","
			}
		}
		if(filterQuery.length == 0) {
			return null
		}
		return filterQuery.substring(0, filterQuery.length - 1)
	}

}

class SearchSettingOption {
	private displayName: string
	private value: string
	private action
	private selected: boolean

	constructor(displayName, value, action, selected) {
		this.displayName = displayName
		this.value = value
		this.action = action
		this.selected = selected
	}

	getDisplayName(): string {
		return this.displayName
	}

	getValue(): string {
		return this.value
	}

	performAction() {
		this.action(this)
	}

	isSelected(): boolean {
		return this.selected
	}

	setSelected(selectedState: boolean) {
		this.selected = selectedState
	}

}

@Injectable()
export class SchoolTypeSetting extends SearchResultSetting {

	private sectionName = "School type"
	private queryKey = "st"
	private inputType = SearchSettingInputType.CHECKBOX
	isLoading = false;

	private preSchoolOption = new SearchSettingOption("Preschool", "prs", this.actionCallback, true)
	private dayCareOption = new SearchSettingOption("Daycare", "dc", this.actionCallback, true)
	private primarySchoolOption = new SearchSettingOption("Primary School", "ps", this.actionCallback, true)
	private middleSchoolOption = new SearchSettingOption("Middle School", "ms", this.actionCallback, true)
	private secondarySchoolOption = new SearchSettingOption("Secondary School", "ss", this.actionCallback, true)
	private seniorSecondarySchoolOption = new SearchSettingOption("Senior Secondary School", "sss", this.actionCallback, true)
	private settingOptionList = [this.preSchoolOption, this.dayCareOption, this.primarySchoolOption, this.middleSchoolOption, 
	this.secondarySchoolOption, this.seniorSecondarySchoolOption]

	constructor() {
		super()
	}

	actionCallback(option) {}

	getInputType(): SearchSettingInputType {
		return this.inputType
	}

	getSectionName(): string {
		return this.sectionName
	}

	getOptionList(): SearchSettingOption[] {
		return this.settingOptionList
	}

	showFilterButton(): boolean {
		return true
	}

	getQueryParamsKey(): string {
		return this.queryKey
	}

	searchableFilterPlaceholder(): string {
		return null
	}

	setQueryDictForRetrival(dict): void{}
}

@Injectable()
export class SchoolBoardSetting extends SearchResultSetting {

	private sectionName = "Board"
	private queryKey = "bd"
	private inputType = SearchSettingInputType.CHECKBOX

	private boardList = null
	private settingOptionList = []
	filter: string
	isLoading = false;
	queryDictStorage = null

	constructor(private appNetworkService: AppNetworkService) {
		super()
		if(!this.boardList) {
			appNetworkService.getBoardList()
			.then(d => {
				let data = d.json();
				this.boardList = data.list;
				this.isLoading = false;

				for(var index in this.boardList) {
					let board = this.boardList[index]
					this.settingOptionList.push(new SearchSettingOption(board["name"], board["id"], this.actionCallback, board["id"] == 1))
				}

				if(this.queryDictStorage) {
					for(var index in this.getOptionList()) {
						var option = this.getOptionList()[index]
						if(this.queryDictStorage[option.getValue()]) {
							option.setSelected(true)
						} else {
							option.setSelected(false)
						}
					}
				}
			}).catch(e => {
				this.isLoading = false;
			});
		}
	}

	actionCallback(option) {}

	getInputType(): SearchSettingInputType {
		return this.inputType
	}

	getSectionName(): string {
		return this.sectionName
	}

	getOptionList(): SearchSettingOption[] {
		return this.settingOptionList
	}

	showFilterButton(): boolean {
		return true
	}

	getQueryParamsKey(): string {
		return this.queryKey
	}

	searchableFilterPlaceholder(): string {
		return "Search Boards"
	}

	setQueryDictForRetrival(dict): void{
		this.queryDictStorage = dict
	}
}

@Injectable()
export class FacilityFiltersSetting extends SearchResultSetting {

	private sectionName = "Facilities"
	private queryKey = "fclt"
	private inputType = SearchSettingInputType.CHECKBOX

	private settingOptionList = []
	queryDictStorage = null
	filter: string

	constructor() {
		super()
		let schoolFacilities = Utils.schoolFacilities
		for(var index in schoolFacilities) {
			let facility = schoolFacilities[index]
			let facilityOption = new SearchSettingOption(facility["name"], facility["code"], this.actionCallback, false)
			this.settingOptionList.push(facilityOption)
		}
		if(this.queryDictStorage) {
			for(var index in this.getOptionList()) {
				var option = this.getOptionList()[index]
				if(this.queryDictStorage[option.getValue()]) {
					option.setSelected(true)
				} else {
					option.setSelected(false)
				}
			}
		}
	}

	actionCallback(option) {}

	getInputType(): SearchSettingInputType {
		return this.inputType
	}

	getSectionName(): string {
		return this.sectionName
	}

	getOptionList(): SearchSettingOption[] {
		return this.settingOptionList
	}

	showFilterButton(): boolean {
		return true
	}

	getQueryParamsKey(): string {
		return this.queryKey
	}

	searchableFilterPlaceholder(): string {
		return "Search Facilities"
	}

	setQueryDictForRetrival(dict): void{
		this.queryDictStorage = dict
	}
}

@Injectable()
class SortBySetting extends SearchResultSetting {

	private sectionName = "Sort By"
	private queryKey = "sb"
	private inputType = SearchSettingInputType.RADIO

	private ratingOption = new SearchSettingOption("Rating", "rt", this.actionCallback, true)
	private relevanceOption = new SearchSettingOption("Relevance", "rc", this.actionCallback, false)
	private seatsAvailableOption = new SearchSettingOption("Seats Available", "sa", this.actionCallback, false)
	private feesOption = new SearchSettingOption("Fees", "fs", this.actionCallback, false)
	private settingOptionList = [this.ratingOption, this.relevanceOption, this.seatsAvailableOption, this.feesOption]

	constructor() {
		super()
	}

	actionCallback(option) {}

	getInputType(): SearchSettingInputType {
		return this.inputType
	}

	getSectionName(): string {
		return this.sectionName
	}

	getOptionList(): SearchSettingOption[] {
		return this.settingOptionList
	}

	showFilterButton(): boolean {
		return false
	}

	getQueryParamsKey(): string {
		return this.queryKey
	}

	searchableFilterPlaceholder(): string {
		return null
	}

	setQueryDictForRetrival(dict): void{}
}

@Injectable()
export class OtherFiltersSetting extends SearchResultSetting {

	private sectionName = "Other Filters"
	private queryKey = "of"
	private inputType = SearchSettingInputType.CHECKBOX

	private admissonsOpenOption = new SearchSettingOption("Admissions Open", "ao", this.actionCallback, false)
	private hasReviewsOption = new SearchSettingOption("Has Reviews", "hr", this.actionCallback, false)
	private settingOptionList = [this.admissonsOpenOption, this.hasReviewsOption]

	constructor() {
		super()
	}

	actionCallback(option) {}

	getInputType(): SearchSettingInputType {
		return this.inputType
	}

	getSectionName(): string {
		return this.sectionName
	}

	getOptionList(): SearchSettingOption[] {
		return this.settingOptionList
	}

	showFilterButton(): boolean {
		return true
	}

	getQueryParamsKey(): string {
		return this.queryKey
	}

	searchableFilterPlaceholder(): string {
		return null
	}

	setQueryDictForRetrival(dict): void{}
}
