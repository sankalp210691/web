export class Coordinates {
	readonly lat: number
	readonly long: number
}