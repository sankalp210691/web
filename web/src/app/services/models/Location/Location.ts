import { Country } from '../../../models/Country';
import { State } from '../../../models/State';
import { City } from '../../../models/City';
import { Locality } from '../../../models/Locality';
import { Coordinates } from './Coordinates';

export class Location {
	readonly coords: Coordinates
	readonly locality: Locality
}