import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
declare var $: any;

@Component({
    template: `
      <h2 class="font-weight-light">{{title}}</h2>
      <p>{{message}}</p>
      <div class="row">
        <div class="col col-md-12 col-lg-12 col-sm-12">
          <button mat-button color="accent" type="button" class="btn btn-rose pull-right" (click)="confirm($event)">
              CONFIRM
          </button>
          <button mat-button type="button" class="btn btn-white pull-right" style="margin-right:20px" (click)="cancel($event)">
              CANCEL
          </button>
        </div>
      </div>
  `
})

export class ConfirmComponent implements OnInit {

  title: string
  message: string
  callback: Function

  constructor(public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any){

    this.title = data.title
    this.message = data.message
    this.callback = data.callback
  }

  ngOnInit() {}

  confirm(event) {
    this.callback(event)
    this.dialogRef.close()
  }

  cancel(event) {
    this.dialogRef.close()
  }
}

@Injectable()
export class AppNotificationService {

  constructor(public dialog: MatDialog) { }

  notifyGenericError() {
    this.notify("Oops! Looks like we encountered some issue. Please try again later",'danger')
  }

  notify(message, type) {
    this.showNotification('top', 'center', type, message)
  }

  confirm(title, message, callback) {
    let dialogRef = this.dialog.open(ConfirmComponent, {
      width: "37.5vw",
      disableClose:true,
      data: { title: title, message: message, callback: callback }
    });
  }

  private showNotification(from, align, type, message){

      $.notify({
          message: message

      },{
          type: type,
          timer: 2000,
          placement: {
              from: from,
              align: align
          },
          template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
            '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss"><i class="material-icons" style="margin-top:10px;color:#fff">close</i></button>' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
              '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
          '</div>'
      });
  }

  ngOnInit() {
  }

}
