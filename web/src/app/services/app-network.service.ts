import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions } from "@angular/http";
import { CookieService } from 'angular2-cookie/core';
import { User } from '../models/User';
import { Student } from '../models/Student';
import { Teacher } from '../models/Teacher';
import { Country } from '../models/Country';
import { State } from '../models/State';
import { City } from '../models/City';
import { Locality } from '../models/Locality';
import { UUID } from 'angular2-uuid';

//school object
export class school {
  constructor(
    public name: string,
    public address: string,
    public city: string,
    public pincode: string,
    public boardId?: number,
    public stateId?: number
    ) {}
}

enum REQUEST_SERVER_TYPE {
  BACKEND,
  SEARCH
}

@Injectable()
export class AppNetworkService {
  private appConfig;
  private _baseUrl = "___BASE_SERVER_URL___";
  private _searchUrl = "___SEARCH_SERVER_URL___";
  private googleMapsApi = "https://maps.googleapis.com/"
  private googleApiKey = "___GOOGLE_API_KEY___"
  private locationCookieKey = "lc"

  private _token: string;
  private _roleauth: string;
  private cookieService:CookieService;
  private versionString: string = "v1";
  private roles = ["ADMIN", "TEACHER", "PARENT"];
  private roleList = null;
  private roleListPromise = null;
  private user = null;
  private boardList = null;

  private countryList: Country[] = []
  private currentCity: City = null

  private stateList = null;
  private cityList = null;
  private locationData = null;
  private localityList = null;

  constructor(
    public http: Http,
    private _cookieService:CookieService
    ) {
    if(!this.getCookie("uid")) {
      this.setPermanentCookie("uid", UUID.UUID())
    }
    this.cookieService = _cookieService;
    if(this.getCookie("authorization") && this.getCookie("authorization").startsWith("Bearer")) {
      this._token = this.getCookie("authorization")
    }
    if(this.getCookie("roleauth") && this.getCookie("roleauth").startsWith("Bearer")) {
      this._roleauth = this.getCookie("roleauth")
    }
    // this.getLocationData(null)
  }

  getGeoCoordinates(callback) {
    if(!callback) {
      return
    }
    if(this.locationData) {
      callback(this.locationData)
    }
    navigator.geolocation.getCurrentPosition(position => {
      this.locationData = {}
      this.locationData["lat"] = position.coords.latitude;
      this.locationData["long"] = position.coords.longitude;

      callback(this.locationData)
    }, error => {
      callback(null)
    });
  }

  //LOCATION RELATED FUNCTIONS
  getLocationData(callback) {

    let country: Country = { id: 1, name: 'India', code: 'IN', internationalPhoneCode: '91', stateList: null }
    this.countryList.push(country)

    let state: State = { id: 17, name: 'Karnataka', country: country, cityList: [] }
    country.stateList = []
    country.stateList.push(state)

    let city: City = { id: 1, name: 'Bengaluru', code: 'Bengaluru', 'state': state, 'localityList': null }
    state.cityList = []
    state.cityList.push(city)
    this.currentCity = city

    this.getLocalityList(city)
    .then(localityList => {
      if(callback) {
        callback(this.currentCity)
      }
    })
    
    //UNCOMMENT THIS WHEN WE GET CLIENT OUTSIDE BANGALORE ... Also create a popup asking for user's city
    // var locationData = this.getCookie(this.locationCookieKey)
    // if(navigator.geolocation && !locationData) {
      //   this.locationData = {}
      // navigator.geolocation.getCurrentPosition(position => {
        //   this.locationData["lat"] = position.coords.latitude;
        //   this.locationData["long"] = position.coords.longitude;
        //   this.getReverseGeocodingData(callback)
        // });
        // } else if(locationData) {
          //   this.locationData = JSON.parse(locationData)
          //   callback(this.locationData)
          // }
        }

        //UNCOMMENT THIS WHEN WE GET CLIENT OUTSIDE BANGALORE ... we might actually not need all this since then we are gonna sk user for his/her city 
        //... but keep this for reference to using google map api and exact address

        // private getReverseGeocodingData(callback) {
          //   let url = this.googleMapsApi + "maps/api/geocode/json?latlng=" + this.locationData["lat"] + "," + this.locationData["long"] + "&key=" + this.googleApiKey

          //   return this.http
          //   .get(url)
          //   .toPromise()
          //   .then(res => {
            //     if(res) {
              //       var data = res.json()
              //       var firstResult = data["results"][0]
              //       var addressComponents = firstResult["address_components"]
              //       for(var index in addressComponents) {
                //         var component = addressComponents[index]
                //         var types = component["types"]
                //         if(types.indexOf("country") > -1) {
                  //           this.locationData["country"] = {
                    //             "name": component["long_name"],
                    //             "code": component["short_name"]
                    //           }
                    //         }
                    //         if(types.indexOf("locality") > -1) {
                      //           this.locationData["city"] = {
                        //             "name": component["long_name"],
                        //             "code": component["short_name"]
                        //           }
                        //         }
                        //         if(types.indexOf("sublocality_level_1") > -1) {
                          //           this.locationData["locality"] = {
                            //             "name": component["long_name"],
                            //             "code": component["short_name"]
                            //           }
                            //         }
                            //       }

                            //       this.setPermanentCookie(this.locationCookieKey, JSON.stringify(this.locationData))
                            //       callback(this.locationData)
                            //     }
                            //   }). catch(ex => {
                              //     console.log(ex)
                              //     callback(null)
                              //   });
                              // }

                              //COOKIE RELATED FUNCTIONS

                              getCookie(key: string) {
                                return this._cookieService.get(key);
                              }

                              setCookieWithExpiry(key: string, val: string, duration: number) {//duration in secs
                                var currentDate = new Date()
                                let opts = {
                                  expires: new Date(currentDate.getTime() + duration)
                                };
                                this._cookieService.put(key, val, opts);
                              }

                              setCookie(key: string, val: string) {
                                this._cookieService.put(key, val);
                              }

                              setPermanentCookie(key: string, val: string) {
                                let opts = {
                                  expires: new Date('3000-12-31')
                                };
                                this._cookieService.put(key, val, opts);
                              }

                              deleteCookie(key: string) {
                                this._cookieService.remove(key);
                              }

                              deleteAllCookies() {
                                this._cookieService.removeAll();
                              }

                              verifyIfLoggedIn(): boolean {
                                if(!this.getCookie("authorization")) {
                                  this.deleteCookie("authorization")
                                  this.deleteCookie("roleauth")
                                  return false
                                }
                                this.getUserRoleAsync()
                                return true
                              }



                              // HTTP REQUESTS WRAPPERS - GET, POST, PATCH

                              //request wrapper
                              getSearchRequest(url): Promise<any> {
                                url = this._searchUrl + url

                                return this.http
                                .get(url)
                                .toPromise()
                                .then(res => {
                                  return res;
                                });
                              }

                              getRequest(url, extraHeaders): Promise<any> {
                                url = this.getUrl(url)
                                var headers = this.getHeaders(url, extraHeaders)

                                return this.http
                                .get(
                                  url,
                                  new RequestOptions({
                                    headers: headers
                                  })
                                  )
                                .toPromise()
                                .then(res => {
                                  if (res.headers.get("roleauth")) {
                                    this._roleauth = res.headers.get("roleauth");
                                    this.setPermanentCookie("roleauth", this._roleauth)
                                  }
                                  return res;
                                });
                              }

                              deleteRequest(url, extraHeaders): Promise<any> {
                                url = this.getUrl(url)
                                var headers = this.getHeaders(url, extraHeaders)

                                return this.http
                                .delete(
                                  url,
                                  new RequestOptions({
                                    headers: headers
                                  })
                                  )
                                .toPromise()
                                .then(res => {
                                  if (res.headers.get("roleauth")) {
                                    this._roleauth = res.headers.get("roleauth");
                                    this.setPermanentCookie("roleauth", this._roleauth)
                                  }
                                  return res;
                                });
                              }

                              postRequest(url, object, extraHeaders): Promise<any> {
                                url = this.getUrl(url)
                                var headers = this.getHeaders(url, extraHeaders)
                                if(extraHeaders) {
                                  for(var key in extraHeaders) {
                                    if(extraHeaders.hasOwnProperty(key)) {
                                      headers[key] = extraHeaders[key]
                                    }
                                  }
                                }
                                return this.http
                                .post(
                                  url,
                                  object,
                                  new RequestOptions({
                                    headers: headers
                                  })
                                  )
                                .toPromise()
                                .then(res => {
                                  if (res.headers.get("authorization")) {
                                    this._token = res.headers.get("authorization");
                                    this.setPermanentCookie("authorization", this._token)
                                  }
                                  if (res.headers.get("roleauth")) {
                                    this._roleauth = res.headers.get("roleauth");
                                    this.setPermanentCookie("roleauth", this._roleauth)
                                  }
                                  return res;
                                });
                              }

                              putRequest(url, object, extraHeaders): Promise<any> {
                                url = this.getUrl(url)
                                var headers = this.getHeaders(url, extraHeaders)
                                if(extraHeaders) {
                                  for(var key in extraHeaders) {
                                    if(extraHeaders.hasOwnProperty(key)) {
                                      headers[key] = extraHeaders[key]
                                    }
                                  }
                                }
                                return this.http
                                .put(
                                  url,
                                  object,
                                  new RequestOptions({
                                    headers: headers
                                  })
                                  )
                                .toPromise()
                                .then(res => {
                                  if (res.headers.get("roleauth")) {
                                    this._roleauth = res.headers.get("roleauth");
                                    this.setPermanentCookie("roleauth", this._roleauth)
                                  }
                                  return res;
                                });
                              }

                              patchRequest(url, object, extraHeaders): Promise<any> {
                                url = this.getUrl(url)
                                var headers = this.getHeaders(url, extraHeaders)
                                if(extraHeaders) {
                                  for(var key in extraHeaders) {
                                    if(extraHeaders.hasOwnProperty(key)) {
                                      headers[key] = extraHeaders[key]
                                    }
                                  }
                                }
                                return this.http
                                .patch(
                                  url,
                                  object,
                                  new RequestOptions({
                                    headers: headers
                                  })
                                  )
                                .toPromise()
                                .then(res => {
                                  if (res.headers.get("roleauth")) {
                                    this._roleauth = res.headers.get("roleauth");
                                    this.setPermanentCookie("roleauth", this._roleauth)
                                  }
                                  return res;
                                });
                              }

                              //upload file
                              uploadFile(file, url, otherParams): Promise<any> {
                                let formData:FormData = new FormData();
                                if(file){ 
                                  formData.append('file', file, file.name);
                                }
                                if(otherParams) {
                                  for(var index in otherParams) {
                                    formData.append(otherParams[index]["key"], otherParams[index]["value"])
                                  }
                                }
                                let headers = new Headers();
                                headers.append('authorization', this._token);
                                headers.append('roleauth', this._roleauth);
                                let options = new RequestOptions({ headers: headers });
                                return this.http.post(this.getUrl(url), formData, options)
                                .toPromise()
                              }

                              //get header for all request
                              getHeaders(url, extraHeaders): any {
                                var _header = {
                                  "Content-Type": "application/json",
                                  "Content-Encoding": "gzip",
                                };

                                if(extraHeaders) {
                                  for(var key in extraHeaders) {
                                    if(extraHeaders.hasOwnProperty(key)) {
                                      _header[key] = extraHeaders[key]
                                    }
                                  }
                                }

                                if(url.indexOf("/secure/app/") > -1) {
                                  _header["authorization"] = this._token
                                  _header["roleauth"] = this._roleauth
                                } else if(url.indexOf("/secure/") > -1) {
                                  _header["authorization"] = this._token
                                }
                                return new Headers(_header);
                              }

                              //get url
                              getUrl(url): string {
                                if(url == "board/list" || url == "state/list" || url == ""){
                                  return this._baseUrl + url;
                                } else {
                                  return this._baseUrl + this.versionString + "/" + url;
                                }

                              }

                              login(phone: string, email: string, password: string): Promise<any> {
                                return this.postRequest("user/login", {
                                  email: email,
                                  phone: phone,
                                  password: password
                                }, null)
                              }

                              signup(user: User): Promise<any> {
                                return this.postRequest("user", {
                                  name: user.name,
                                  email: user.email,
                                  phone: user.phone,
                                  password: user.password
                                }, null)
                              }

                              forgotPassword1(phone: string): Promise<any> {
                                return this.patchRequest("user/forgotpassword", {
                                  phone: phone
                                }, null)
                              }

                              forgotPassword2(phone: string, otp: string): Promise<any> {
                                return this.postRequest("user/forgotpassword", {
                                  phone: phone,
                                  otp: otp
                                }, null)
                              }

                              verifyUser(userId: string, otp: string): Promise<any> {
                                return this.postRequest("user/verify",{
                                  userId: userId,
                                  otp: otp
                                }, null)
                              }

                              updatePasswordWithoutLogin(newPassword: string, repeatNewPassword: string, fpauthorization: string): Promise<any> {
                                var fpauthHeader = {
                                  "fpauthorization": fpauthorization
                                }
                                return this.patchRequest("user/updatePassword",{
                                  newPassword: newPassword,
                                  repeatNewPassword: repeatNewPassword
                                }, fpauthHeader)
                              }

                              updatePassword(oldPassword: string, newPassword: string, repeatNewPassword: string): Promise<any> {
                                return this.patchRequest("secure/user/updatePassword",{
                                  oldPassword: oldPassword,
                                  newPassword: newPassword,
                                  repeatNewPassword: repeatNewPassword
                                }, null)
                              }

                              resendOtp(phone: string): Promise<any> {
                                return this.postRequest("user/resendOtp", {
                                  phone: phone
                                }, null)
                              }

                              //get all user details
                              getUserAsync(): Promise<any> {
                                if(this.user == null) {
                                  return this.getRequest("secure/user", null)
                                  .then(d => {
                                    let data = d.json();
                                    this.user = data;
                                    return data;
                                  }).catch(e => {
                                    this.deleteCookie("authorization");
                                    this.deleteCookie("roleauth");
                                    return false;
                                  });
                                }
                              }

                              getUser(): User {
                                return this.user;
                              }

                              //logout
                              logout() {
                                this.deleteCookie("authorization")
                                this.deleteCookie("roleauth")
                                document.location.href="/";
                              }

                              //get all boards
                              getBoardList(): Promise<any> {
                                if(this.boardList == null) {
                                  return this.getRequest("/board/list", null)
                                } else {
                                  return new Promise(this.boardList);
                                }
                              }

                              //get all states
                              getStateList(): Promise<any> {
                                if(this.stateList == null) {
                                  return this.getRequest("state/list", null)
                                } else {
                                  return new Promise(this.stateList);
                                }
                              }

                              getCityList(stateId): Promise<any> {
                                if(this.cityList == null) {
                                  return this.getRequest("state/" + stateId + "/city/list", null)
                                } else {
                                  return new Promise(this.cityList);
                                }
                              }

                              //get localities of a city
                              getLocalityList(city): Promise<Locality[]> {
                                if(city.localityList && city.localityList.length != 0) {
                                  return new Promise((resolve, reject) => {
                                    resolve(this.localityList)
                                  });
                                } else {
                                  return this.getRequest("city/" + city.id + "/locality/list", null)
                                  .then(d => {
                                    let localityList: Locality[] = d.json().list
                                    city.localityList = localityList
                                    return localityList
                                  }, error => {
                                    console.log(error)
                                    return null
                                  })
                                }
                              }

                              //get all user roles
                              getUserRoleAsync(): Promise<any> {
                                if(this.roleList) {
                                  return new Promise((resolve, reject) => {
                                    resolve(this.roleList)
                                  });
                                } else {
                                  if(this.roleListPromise) {
                                    return this.roleListPromise
                                  } else {
                                    this.roleListPromise = this.getRequest("secure/user/listRoles", null)
                                    .then(d => {
                                      let data = d.json();
                                      this.roleList = data;
                                      return data
                                    })
                                    .catch(e => {
                                      this.deleteCookie("authorization");
                                      this.deleteCookie("roleauth");
                                      this.roleListPromise = null;
                                      return null
                                    });
                                    return this.roleListPromise
                                  }
                                }
                              }

                              //save school
                              saveSchoolDetail(objSchool): Promise<any> {
                                if(objSchool["id"]) {//edit
                                  return this.putRequest("secure/school/", objSchool, null);
                                } else {//create
                                  var file = objSchool["displayPicImage"];
                                  var otherParams = [];
                                  otherParams.push({"key": "name", "value": objSchool["name"]})
                                  otherParams.push({"key": "pincode", "value": objSchool["pincode"]})
                                  otherParams.push({"key": "address", "value": objSchool["address"]})

                                  if(objSchool["boardId"]) {
                                    otherParams.push({"key": "boardId", "value": objSchool["boardId"]})
                                  }
                                  otherParams.push({"key": "instituteCategoryList", "value": JSON.stringify(objSchool["instituteCategoryList"])})
                                  otherParams.push({"key": "localityId", "value": objSchool["locality"]["id"]})
                                  return this.uploadFile(file, "secure/school/", otherParams);
                                }
                              }

                              //save school profile/cover pic
                              saveSchoolProfileCoverPic(file, type, schoolId): Promise<any> {
                                var otherParams = [{"key":"pictureType", "value": type}, {"key":"schoolId", "value": schoolId}]
                                return this.uploadFile(file, "secure/school/pcpic", otherParams)
                              }

                              getSchools(filters, offset: number): Promise<any> {
                                var queryString = ""
                                for(var key in filters) {
                                  if(filters.hasOwnProperty(key) && filters[key] && key != "pgc") {
                                    queryString += key + "=" + filters[key] + "&"
                                  }
                                }
                                queryString += "offset=" + offset
                                return this.getSearchRequest("v1/school?" + queryString)
                              }

                              schoolSearchAutocomplete(query, selectedLocationId, isCity) {
                                var queryString = ""
                                queryString += "q=" + query + "&"
                                if(isCity) {
                                  queryString += "cid=" + selectedLocationId
                                } else {
                                  queryString += "lid=" + selectedLocationId
                                }
                                return this.getSearchRequest("v1/school/autocomplete?" + queryString)
                              }

                              //get school details
                              getSchool(schoolId): Promise<any> {
                                if(this.verifyIfLoggedIn()) {
                                  return this.getRequest("secure/school/" + schoolId, null);
                                } else {
                                  return this.getRequest("school/" + schoolId, null)
                                }
                              }

                              //create review
                              submitReview(rating, review, schoolId, associationType, isAnonymous): Promise<any> {
                                if(!this.verifyIfLoggedIn()) {
                                  return;
                                }
                                let reviewObject = {
                                  rating: rating,
                                  review: review,
                                  schoolId: schoolId,
                                  associationType: associationType,
                                  isAnonymous: isAnonymous
                                }
                                return this.postRequest("secure/review", reviewObject, null);
                              }

                              //edit review
                              editReview(reviewId, rating, review, schoolId, associationType, isAnonymous): Promise<any> {
                                if(!this.verifyIfLoggedIn()) {
                                  return;
                                }
                                let reviewObject = {
                                  id: reviewId,
                                  rating: rating,
                                  review: review,
                                  schoolId: schoolId,
                                  associationType: associationType,
                                  isAnonymous: isAnonymous
                                }
                                return this.patchRequest("secure/review", reviewObject, null);
                              }

                              //get school reviews
                              getSchoolReviews(schoolId, startDate, endDate, offset, filterByUser, filterBySchool): Promise<any> {
                                if(this.verifyIfLoggedIn()) {
                                  if(!filterBySchool) {
                                    filterBySchool = false;
                                  }
                                  if(!filterByUser) {
                                    filterByUser = false;
                                  }
                                  return this.getRequest("secure/review/?schoolId=" + schoolId + "&startDate=" 
                                    + startDate + "&endDate=" + endDate + "&offset=" + offset 
                                    + "&filterByUser=" + filterByUser + "&filterBySchool=" + filterBySchool, null)
                                } else {
                                  return this.getRequest("review/?schoolId=" + schoolId + "&startDate=" + startDate + "&endDate=" + endDate + "&offset=" + offset, null)
                                }
                              }

                              //save class detail
                              saveClassDetail(objClass, schoolId): Promise<any> {
                                return this.postRequest("secure/app/class",  objClass, null);
                              }

                              editTeacher(teacher: Teacher, callback) {
                                this.putRequest("secure/app/teacher/" + teacher.id, teacher, null)
                                .then(data => {
                                  data = data.json()
                                  callback(null, {'teacher': data.map.teacher})
                                })
                                .catch(e => {
                                  callback(e, null)
                                })
                              }

                              createTeacher(teacher: Teacher, callback) {
                                this.postRequest("secure/app/teacher", teacher, null)
                                .then(data => {
                                  data = data.json()
                                  callback(null, {'teacher': data.map.teacher})
                                })
                                .catch(e => {
                                  callback(e, null)
                                })
                              }

                              deleteTeacher(teacherId) {
                                return this.deleteRequest("secure/app/teacher/" + teacherId, null)
                              }

                              editStudent(student: Student, callback) {
                                var file = student.profilePicUrl
                                if(file && typeof file !== "string") {
                                  var uploadRequest = this.uploadFile(
                                    file,
                                    "secure/app/student/profilepic",
                                    null
                                    )
                                  .then(d => {
                                    var imageUrl = d.headers.get("Location")
                                    if(imageUrl) {
                                      student.profilePicUrl = imageUrl
                                      this.putRequest("secure/app/student/" + student.id, student, null)
                                      .then(data => {
                                        data = data.json()
                                        callback(null, {"profilePicUrl": imageUrl, student: data.map.student})
                                      })
                                      .catch(e => {
                                        callback(e, null)
                                      })
                                    } else {
                                      var r = {"status": 500}
                                      callback(r, null)
                                    }
                                  })
                                  .catch(e => {
                                    callback(e, null)
                                  })
                                } else {
                                  this.putRequest("secure/app/student/" + student.id , student, null)
                                  .then(data => {
                                    data = data.json()
                                    callback(null, {'student': data.map.student})
                                  })
                                  .catch(e => {
                                    callback(e, null)
                                  })
                                }
                              }

                              createStudent(student: Student, callback) {
                                var file = student.profilePicUrl
                                if(file) {
                                  var uploadRequest = this.uploadFile(
                                    file,
                                    "secure/app/student/profilepic",
                                    null
                                    )
                                  .then(d => {
                                    var imageUrl = d.headers.get("Location")
                                    if(imageUrl) {
                                      student.profilePicUrl = imageUrl
                                      this.postRequest("secure/app/student", student, null)
                                      .then(data => {
                                        data = data.json()
                                        callback(null, {"profilePicUrl": imageUrl, student: data.map.student})
                                      })
                                      .catch(e => {
                                        callback(e, null)
                                      })
                                    } else {
                                      var r = {"status": 500}
                                      callback(r, null)
                                    }
                                  })
                                  .catch(e => {
                                    callback(e, null)
                                  })
                                } else {
                                  this.postRequest("secure/app/student", student, null)
                                  .then(data => {
                                    data = data.json()
                                    callback(null, {'student': data.map.student})
                                  })
                                  .catch(e => {
                                    callback(e, null)
                                  })
                                }
                              }

                              getStudentsBySchool() {
                                return this.getRequest("secure/app/student", null)
                              }

                              getTeachersBySchool() {
                                return this.getRequest("secure/app/teacher", null)
                              }

                              getClassesBySchool() {
                                return this.getRequest("secure/app/class/", null)
                              }

                              deleteStudent(studentId) {
                                return this.deleteRequest("secure/app/student/" + studentId, null)
                              }

                              increaseStage() {
                                return this.patchRequest("secure/app/school/stage", null, null)
                              }

                              applyToSchool(schoolId): Promise<any> {
                                return this.postRequest("secure/school/" + schoolId + "/apply", null, null)
                              }

                              offlineApplyToSchool(parentName, email, phone, schoolId): Promise<any> {
                                let json = {
                                  name: parentName,
                                  email: email,
                                  phone: "+91-" + phone
                                }
                                return this.postRequest("school/"+schoolId+"/apply", json, null)
                              }

                              // FILE UPLOADS

                              uploadStudentFile(uri, schoolId): Promise<any> {
                                return this.uploadFile(
                                  uri,
                                  "secure/app/user/student",
                                  null
                                  );
                              }

                              teacherFileUpload(uri, schoolId): Promise<any> {
                                return this.uploadFile(
                                  uri,
                                  "secure/app/user/teacher",
                                  null
                                  );
                              }

                              studentTeacherMappingFileUpload(uri, schoolId): Promise<any> {
                                return this.uploadFile(
                                  uri,
                                  "secure/app/user/stmapping",
                                  null
                                  );
                              }

                              studentTeacherMappingSave(reqObj): Promise<any> {
                                return this.postRequest("secure/app/sctMapping", reqObj, null)
                              }

                              //role auth key
                              getRoleAuthKey(type, id, sid): Promise<boolean> {
                                return this.getRequest(
                                  ("secure/user/roleauth/" + type + "/" + id + "?sid=" + sid), null
                                  )
                                .then(d => {
                                  this._roleauth = d.headers.get("roleauth");
                                  return true;
                                })
                                .catch(e => {
                                  return false;
                                });
                              }

                            }