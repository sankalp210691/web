import { Component} from '@angular/core';
import { AppNetworkService } from './services/app-network.service';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { LoginComponent } from './components/login/login.component';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	private _router: Subscription;
	private appNetworkService: AppNetworkService;
	public isSideBarActive: Boolean = false;
	isLoggedIn: boolean = false

	private authRoutePrefixMap = {
		'authorized': ['/dashboard', '/user-profile']
	}

	constructor(private apns: AppNetworkService, private router: Router, public dialog: MatDialog) { 
		this.appNetworkService = apns;

		router.events.subscribe((val) => {
			this.isLoggedIn = this.appNetworkService.verifyIfLoggedIn()
			if(val instanceof NavigationEnd) {
				if(this.shouldRedirectToHome()) {
					document.location.href="/";
				} else {
					if(val.url.indexOf("/dashboard") > -1) {
						this.appNetworkService.getUserRoleAsync()
						.then(roleList => {
							if(roleList && (roleList["ADMIN"] || roleList["TEACHER"] || roleList["PARENT"])) {
								this.isSideBarActive = true;
							} else {
								this.isSideBarActive = false;
							}
						})
						
					} else {
						this.isSideBarActive = false;
					}
				}
			}
		});
	}

	private shouldRedirectToHome() {
		var isAuthorizedRoute = false
		for(var index in this.authRoutePrefixMap['authorized']) {
			var prefix = this.authRoutePrefixMap['authorized'][index]
			if(window.location.pathname.startsWith(prefix)) {
				isAuthorizedRoute = true
				break
			}
		}
		return isAuthorizedRoute && !this.isLoggedIn
	}

	showAuthModal() {
		let dialogRef = this.dialog.open(LoginComponent, {
			width: "30vw"
		});
	}

	logout() {
		this.appNetworkService.logout()
	}
}