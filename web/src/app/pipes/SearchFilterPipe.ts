import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchfilter',
    pure: false
})
export class SearchFilterPipe implements PipeTransform {
    transform(items: any[], filter: Object): any {
        if (!items || !filter) {
            return items;
        }
        // filter items array, items which match and return true will be
        // kept, false will be filtered out
        return items.filter(item => {
            for(var key in filter) {
                if(filter.hasOwnProperty(key)) {
                    if(!filter[key]) {
                        return true
                    }
                    if(item[key].trim().toLowerCase().indexOf(filter[key].trim().toLowerCase()) > -1) {
                        return true
                    }
                }
            }
            return false
        });
    }
}