import { Component, OnInit, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { AgmCoreModule } from '@agm/core';
import { AppNotificationService } from '../services/app-notification.service';
import { AppNetworkService } from '../services/app-network.service';
import { School } from '../models/School';
import { Router, ActivatedRoute } from '@angular/router';
import { ViewChild } from '@angular/core';
import { Utils } from '../Utils'
import { OfflineApplyToSchoolComponent } from '../components/offlineapplytoschool/offlineApplyToSchool.component';
import { SchoolInfoComponent } from '../components/schoolinfo/schoolinfo.component';
import { LoginComponent } from '../components/login/login.component';
import { RatingReviewComponent } from '../components/rating-review/rating-review.component';
import { ImageEditorComponent } from '../components/image-editor/image-editor.component';

@Component({
	selector: 'app-school-profile',
	templateUrl: './school-profile.component.html',
	styleUrls: ['./school-profile.component.css']
})
export class SchoolProfileComponent implements OnInit {

	@ViewChild('displayPicInput')
	displayPicInput: ElementRef;

	@ViewChild('coverPicInput')
	coverPicInput: ElementRef;

	school = {"displayPic": "../../assets/img/dummy-school-dp.jpg", "coverPic": "../../assets/img/default_school_coverpic.jpeg"}
	isCurrentUserAdminOfSchool = false;
	SCHOOL_PROFILE_PICTURE_TYPE = "profile_pic"
	SCHOOL_COVER_PICTURE_TYPE = "cover_pic"
	profilePicUploadInProgress = false;
	coverPicUploadInProgress = false;
	userAlreadyShownInterest = false;

	constructor(private appNotificationService: AppNotificationService, private appNetworkService: AppNetworkService,
		private route: ActivatedRoute, public dialog: MatDialog) {}

	zoom = 17
	private maxFacilityColumnCount = 4
	label = "Maharaja Agrasen Vidyalaya"
	socialButtons = []
	reviews = []
	userReview = null
	schoolFacilities = []

	getDirections() {
		var url = "https://www.google.co.in/maps/dir//" + this.school["latitude"] + "," + this.school["longitude"]
		window.open(url, '_blank').focus()
	}

	isUserLoggedIn() {
		return this.appNetworkService.verifyIfLoggedIn();
	}

	allowedToReview() {
		return this.isUserLoggedIn() && !this.isCurrentUserAdminOfSchool
	}

	showAuthModal() {
		let dialogRef = this.dialog.open(LoginComponent, {
			width: "30vw"
		});
	}

	showReviewModal() {
		let dialogRef = this.dialog.open(RatingReviewComponent, {
			width: "40vw",
			maxHeight: "600px",
			data: { schoolId: this.school["id"], schoolName: this.school["name"] }
		});

		dialogRef.afterClosed().subscribe(ratingReview => {
			if(ratingReview) {
				this.reviews.unshift(ratingReview)
				this.userReview = ratingReview
			}
		});
	}

	applyToSchool(event) {

		if(!this.school["id"]) {
			return;
		}

		var button = event.target
		var originalTargetHtml = button.innerHTML
		Utils.markBusy(button)

		if(this.appNetworkService.verifyIfLoggedIn()) {
			this.appNetworkService.applyToSchool(this.school["id"])
			.then(d => {
				this.appNotificationService.notify("Your interest has been successfully submitted", "info")
				this.userAlreadyShownInterest = true
				Utils.markActive(button, originalTargetHtml)
			}).catch(error => {
				this.appNotificationService.notify("Your interest could not be submitted. Please try again later", "danger")
				Utils.markActive(button, originalTargetHtml)
			})
			//do error handeling and all
		} else {
			let dialogRef = this.dialog.open(OfflineApplyToSchoolComponent, {
				width: "40vw",
				data: { schoolId: this.school["id"], schoolName: this.school["name"] }
			});

			dialogRef.afterClosed().subscribe(result => {
				Utils.markActive(button, originalTargetHtml)
			});
		}
	}

	openEditSchoolForm() {
		let schoolClone = JSON.parse(JSON.stringify(this.school))
		let dialogRef = this.dialog.open(SchoolInfoComponent, {
			width: "45vw",
			minWidth: "500px",
			maxHeight: "70vh",
			data: { school: schoolClone }
		});

		dialogRef.afterClosed().subscribe(result => {
			if(result) {
				this.getSchoolInfo(this.school["id"])
				this.appNotificationService.notify("Updating details...", "info")
			}
		});
	}

	isValidImageFile(fileType) {
		var ftype = fileType.split("/")[1]
		if(ftype) {
			if(!Utils.getSupportedFileInfo()[ftype]) {
				return false
			}
			return Utils.getSupportedFileInfo()[ftype]["normalizedType"] == "image"
		}
		return false;
	}

	showImageCropper(event, type) {
		if(this.appNetworkService.verifyIfLoggedIn()) {
			let file = event.target.files[0]
			if(this.isValidImageFile(file.type)) {
				let dialogRef = this.dialog.open(ImageEditorComponent, {
					width: "40vw",
					height: "30vw",
					data: { file: event, aspectRatio: type == this.SCHOOL_PROFILE_PICTURE_TYPE ? "1" : "6.7939393939" }
				});

				dialogRef.afterClosed().subscribe(result => {
					if(!result) {
						if(type == this.SCHOOL_PROFILE_PICTURE_TYPE) {
							this.displayPicInput.nativeElement.value = null;
						} else {
							this.coverPicInput.nativeElement.value = null;
						}
					} else {
						let ff = Utils.dataURLtoFile(result, file.name)
						if(type == this.SCHOOL_PROFILE_PICTURE_TYPE) {
							this.uploadProfilePic(ff)
						} else {
							this.uploadCoverPic(ff)
						}
					}
				});
			} else {
				this.appNotificationService.notify("Invalid image. Please upload a valid image", "danger")
			}
		}
	}

	uploadProfilePic(file) {
		this.profilePicUploadInProgress = true
		this.appNetworkService.saveSchoolProfileCoverPic(file, this.SCHOOL_PROFILE_PICTURE_TYPE, this.school["id"])
		.then(result => {
			this.profilePicUploadInProgress = false
			this.getSchoolInfo(this.school["id"])
			this.appNotificationService.notify("Profile picture successfully updated", "info")
		}).catch(error => {
			this.profilePicUploadInProgress = false
			this.appNotificationService.notify("Could not update profile picture. Please try again later", "danger")
		})
	}

	uploadCoverPic(file) {
		this.coverPicUploadInProgress = true
		this.appNetworkService.saveSchoolProfileCoverPic(file, this.SCHOOL_COVER_PICTURE_TYPE, this.school["id"])
		.then(result => {
			this.coverPicUploadInProgress = false
			this.getSchoolInfo(this.school["id"])
			this.appNotificationService.notify("Cover picture successfully updated", "info")
		}).catch(error => {
			this.coverPicUploadInProgress = false
			this.appNotificationService.notify("Could not update cover picture. Please try again later", "danger")
		})
	}

	getSchoolInfo(schoolId) {
		this.appNetworkService.getSchool(schoolId)
		.then(data => {
			if(!data.json().searchable && data.json().stage < 5){
				this.appNotificationService.notify("This school does not exist!","danger")
				return;
			}
			this.school = data.json()
			if(!this.school["displayPic"]) {
				this.school["displayPic"] = "../../assets/img/dummy-school-dp.jpg"
			}
			if(!this.school["coverPic"]) {
				this.school["coverPic"] = "../../assets/img/default_school_coverpic.jpeg"
			}
			if(this.appNetworkService.verifyIfLoggedIn()) {
				this.isCurrentUserAdminOfSchool = this.school["currentUserAdminId"] ? true: false;
			}
			if(this.school["facilities"]) {
				this.schoolFacilities = []
				var row = 0, col = 0;
				for(var index in this.school["facilities"]) {
					if(!this.schoolFacilities[row]) {
						this.schoolFacilities[row] = []
					}
					this.schoolFacilities[row][col] = Utils.getSchoolFacilityByCode(this.school["facilities"][index])
					col++
					if(col == this.maxFacilityColumnCount) {
						col = 0
						row++
					}
				}
			}

			this.socialButtons = [
			{
				"href": "https://www.facebook.com/sharer.php?t=" + encodeURIComponent(this.school["name"]) + "&u=" + encodeURI(window.location.href),
				"style": { 'font-size': '20px', 'text-align': 'center', 'padding': '15px 5px', 'width': '50px', 'height': '45px', 'background-color': '#3b5998', 'color': '#fff' },
				"fontawesomeClass": "fab fa-facebook-f"
			},
			{
				"href": "https://twitter.com/intent/tweet?text="+ encodeURIComponent(this.school["name"]) + "&url=" + encodeURI(window.location.href),
				"style": { 'font-size': '20px', 'text-align': 'center', 'padding': '15px 5px', 'width': '50px', 'height': '45px', 'background-color': '#1da1f2', 'color': '#fff' },
				"fontawesomeClass": "fab fa-twitter"
			},
			{
				"href": "https://plus.google.com/share?url=" + encodeURI(window.location.href),
				"style": { 'font-size': '20px', 'text-align': 'center', 'padding': '15px 5px', 'width': '50px', 'height': '45px', 'background-color': '#d34836', 'color': '#fff' },
				"fontawesomeClass": "fab fa-google-plus-g"
			},
			{
				"href": "https://www.facebook.com/dialog/send?link=" + encodeURI(window.location.href) + "&redirect_uri=" + encodeURI(window.location.href) + "&app_id=521270401588372",
				"style": { 'font-size': '20px', 'text-align': 'center', 'padding': '15px 5px', 'width': '50px', 'height': '45px', 'background-color': '#0084ff', 'color': '#fff' },
				"fontawesomeClass": "fab fa-facebook-messenger"
			},
			{
				"href": "https://pinterest.com/pin/create/button/?description=" + encodeURIComponent(this.school["name"]) + "&url=" + encodeURI(window.location.href),
				"style": { 'font-size': '20px', 'text-align': 'center', 'padding': '15px 5px', 'width': '50px', 'height': '45px', 'background-color': '#bd081c', 'color': '#fff' },
				"fontawesomeClass": "fab fa-pinterest-p"
			},
			{
				"href": "https://web.whatsapp.com/send?text=" + encodeURI(window.location.href),
				"style": { 'font-size': '20px', 'text-align': 'center', 'padding': '15px 5px', 'width': '50px', 'height': '45px', 'background-color': '#25d366', 'color': '#fff' },
				"fontawesomeClass": "fab fa-whatsapp"
			},
			{
				"href": "https://www.linkedin.com/sharing/share-offsite?title=" + encodeURIComponent(this.school["name"]) + "&url=" + encodeURI(window.location.href),
				"style": { 'font-size': '20px', 'text-align': 'center', 'padding': '15px 5px', 'width': '50px', 'height': '45px', 'background-color': '#0077b5', 'color': '#fff' },
				"fontawesomeClass": "fab fa-linkedin-in"
			}
			]
		})
		.catch(e => {
			if(e.status >= 500) {
				this.appNotificationService.notifyGenericError()
			}
			console.log(e)
		})
	}

	getSchoolReviews(schoolId) {
		let startDate = Utils.getOldDateString();
		let endDate = Utils.getFutureDateString();
		this.appNetworkService.getSchoolReviews(schoolId, startDate, endDate, 0, false, true)
		.then(result => {
			result = result.json()
			if(result && result["map"] && result["map"]["reviews"]) {
				this.reviews = result["map"]["reviews"]	
			}
		}).catch(error => {
			if(error.status >= 500) {
				this.appNotificationService.notifyGenericError()
			}
			console.log(error)
		})
	}

	getUserSchoolReview(schoolId, userId) {
		let startDate = Utils.getOldDateString();
		let endDate = Utils.getFutureDateString();
		this.appNetworkService.getSchoolReviews(schoolId, startDate, endDate, 0, true, true)
		.then(result => {
			result = result.json()
			if(result && result["map"] && result["map"]["reviews"]) {
				this.userReview = result["map"]["reviews"][0]
			}
		}).catch(error => {
			if(error.status >= 500) {
				this.appNotificationService.notifyGenericError()
			}
			console.log(error)
		})
	}

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.getSchoolInfo(params['id'])
			this.getSchoolReviews(params['id'])
			if(this.appNetworkService.verifyIfLoggedIn()) {
				this.getUserSchoolReview(params['id'], this.appNetworkService.getUser().id)
			}
			let user = this.appNetworkService.getUser()
			if(user && user["interestedSchools"]) {
				if(user["interestedSchools"].indexOf(+params['id']) > -1) {
					this.userAlreadyShownInterest = true;
				}
			}
		})
	}

}
