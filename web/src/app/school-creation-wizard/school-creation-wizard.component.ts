import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { AppNetworkService } from '../services/app-network.service';
import { AppNotificationService } from '../services/app-notification.service';
import { MatIconModule } from '@angular/material/icon';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule, MatTableModule, MatTableDataSource, MatSelect, MatCheckbox } from '@angular/material';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { Utils } from '../Utils'
import { Router, ActivatedRoute } from '@angular/router';
import { School } from '../models/School';
import { Student } from '../models/Student';
import { Teacher } from '../models/Teacher';
import { Subject } from '../models/Subject';
import { InstituteCategory } from '../models/InstituteCategory';
import { StudentInputComponent } from '../components/studentinput/studentinput.component';
import { SchoolInfoComponent } from '../components/schoolinfo/schoolinfo.component';

declare var $: any;

@Component({
  selector: 'app-school-creation-wizard',
  templateUrl: './school-creation-wizard.component.html',
  styleUrls: ['./school-creation-wizard.component.css']
})
export class SchoolCreationWizardComponent implements OnInit {

  private appNetworkService: AppNetworkService;
  private appNotificationService: AppNotificationService;
  public school: School = new School();
  public adminId: Number;
  
  private maxSectionCount = 20
  private minSectionCount = 1
  public sectionCountArray: Number[] = [];
  private router: Router;
  public isDataAvailable:boolean = false;
  public classStructure;
  private uploadedFile;

  //keyin variables
  private students:Student[]
  private teachers:Teacher[]
  private subjects:Subject[]
  private classInfo:Object[]

  constructor(private apns: AppNetworkService, private ans: AppNotificationService,
    public dialog: MatDialog, private rtr: Router, private route: ActivatedRoute,
    public studentInputComponent: StudentInputComponent, public schoolInfoComponent: SchoolInfoComponent) { 

  	this.appNetworkService = apns;
  	this.appNotificationService = ans;
    this.router = rtr;

    for(var index = this.minSectionCount; index <= this.maxSectionCount; index++) {
      this.sectionCountArray.push(index)
    }
    this.classStructure = [{
      "levelName": "Preschool",
      "structure": [
      {
        "standard": "Pre-Nursery",
        "selected": false,
        "sectionCount": 1
      },
      {  
        "standard": "Nursery",
        "selected": false,
        "sectionCount": 1
      },
      {
        "standard": "U.K.G",
        "selected": false,
        "sectionCount": 1
      },
      {
        "standard": "L.K.G",
        "selected": false,
        "sectionCount": 1
      }
      ]
    }];
    this.classStructure.push({
      "levelName": "Middle School",
      "structure": [
      {
        "standard": "1st",
        "selected": false,
        "sectionCount": 1
      },
      {
        "standard": "2nd",
        "selected": false,
        "sectionCount": 1
      },
      {
        "standard": "3rd",
        "selected": false,
        "sectionCount": 1
      }
      ]
    })
    for(var index = 4; index <= 5; index++) {
      this.classStructure[1].structure.push({
        "standard": index + "th",
        "selected": false,
        "sectionCount": 1
      })
    }
    this.classStructure.push({
      "levelName": "High School",
      "structure": []
    })
    for(var index = 6; index <= 12; index++) {
      this.classStructure[2].structure.push({
        "standard": index + "th",
        "selected": false,
        "sectionCount": 1
      })
    }
  }

  onSchoolInfoUpdate(event) {
    this.school = event
    this.router.navigate([this.school.id],{relativeTo:this.route});
  }

  onSubjectMappingInput(event) {
    if(event.file) {
      this.uploadedFile = event.file
      this.subjects = null
      this.classInfo = null
    } else {
      this.uploadedFile = null
      this.classInfo = event.classInfo
    }
  }

  onTeacherInput(event) {
    if(event.teachers) {
      this.teachers = event.teachers
      this.uploadedFile = null
    } else if(event.file) {
      this.uploadedFile = event.file
      this.teachers = null
    }
  }


  onStudentInput(event) {
    if(event.students) {
      this.students = event.students
      this.uploadedFile = null
    } else if(event.file) {
      this.uploadedFile = event.file
      this.students = null
    }
  }

  abort(event) {
    var button = event.target
    var originalTargetHtml = button.innerHTML
    Utils.markBusy(button)
    this.appNetworkService.deleteCookie("roleauth")
    this.router.navigateByUrl("/dashboard")
    Utils.markActive(button, originalTargetHtml)
  }

  next(event, nextStage) {
    var button = event.target
    var originalTargetHtml = button.innerHTML
    Utils.markBusy(button)
    switch(nextStage) {
      case 3: this.networkStage3(event.target, success => {
        if(success){
          this.ngOnInit()
        } else {
          Utils.markActive(button, originalTargetHtml)
        }
      })
      break;
      case 4: this.networkStage4(event.target, success => {
        if(success){
          this.ngOnInit()
        } else {
          Utils.markActive(button, originalTargetHtml)
        }
      })
      break;
      case 5: this.networkStage5(event.target, success => {
        if(success){
          this.router.navigateByUrl("/dashboard")
        } else {
          Utils.markActive(button, originalTargetHtml)
        }
      })
      break;
    }
  }

  private networkStage3(target, callback) {
    if(!this.uploadedFile && !this.teachers) {
      this.appNotificationService.notify("Please upload the teacher data CSV file or key in teacher's data","danger")
      callback(false)
    } else if(this.uploadedFile) {
      if(!Utils.checkFileExtention(this.uploadedFile.name, "csv")) {
        this.appNotificationService.notify("Please upload the teacher data CSV file","danger")
        callback(false)
      } else {
        this.appNetworkService.teacherFileUpload(this.uploadedFile, this.school.id)
        .then(d => {
          callback(true);
        })
        .catch(e => {
          console.log(e)
          if(e.status >= 500) {
            this.appNotificationService.notifyGenericError()
          }else if(e.status == 400 ) {
            var error = JSON.parse(e._body).map.error
            this.appNotificationService.notify(error,"danger")
          } else if(e.status == 401){
            this.appNetworkService.deleteAllCookies()
            window.location.reload();
          }
          callback(false)
        })
      }
    } else if(this.teachers && this.teachers.length > 0) {
      this.appNetworkService.increaseStage()
      .then(d => {
        callback(true);
      })
      .catch(e => {
        if(e.status >= 500) {
          this.appNotificationService.notifyGenericError()
        }else if(e.status == 400 ) {
          var error = JSON.parse(e._body).map.error
          this.appNotificationService.notify(error,"danger")
        } else if(e.status == 401){
          this.appNetworkService.deleteAllCookies()
          window.location.reload();
        }
        callback(false)
      })
    } else {
      this.appNotificationService.notify("You must add atleast one teacher","danger")
      callback(false)
    }
  }

  private networkStage4(target, callback) {
    if(!this.uploadedFile && !this.students) {
      this.appNotificationService.notify("Please upload the student data CSV file or key in students' data","danger")
      callback(false)
    } else if(this.uploadedFile) {
      this.appNetworkService.uploadStudentFile(this.uploadedFile, this.school.id)
      .then(d => {
        callback(true);
      })
      .catch(e => {
        if(e.status >= 500) {
          this.appNotificationService.notifyGenericError()
        }else if(e.status == 400 ) {
          var error = JSON.parse(e._body).map.error
          this.appNotificationService.notify(error,"danger")
        } else if(e.status == 401){
          this.appNetworkService.deleteAllCookies()
          window.location.reload();
        }
        callback(false)
      })
    } else if(this.students && this.students.length > 0) {
      this.appNetworkService.increaseStage()
      .then(d => {
        callback(true);
      })
      .catch(e => {
        if(e.status >= 500) {
          this.appNotificationService.notifyGenericError()
        }else if(e.status == 400 ) {
          var error = JSON.parse(e._body).map.error
          this.appNotificationService.notify(error,"danger")
        } else if(e.status == 401){
          this.appNetworkService.deleteAllCookies()
          window.location.reload();
        }
        callback(false)
      })
    } else {
      this.appNotificationService.notify("You must add atleast one student","danger")
      callback(false)
    }
  }

  private networkStage5(target, callback) {
    if(!this.uploadedFile && !this.classInfo) {
      this.appNotificationService.notify("Please upload the subject data CSV file or key in subject data", "danger")
      callback(false)
    } else if(this.uploadedFile) {
      if(!Utils.checkFileExtention(this.uploadedFile.name, "csv")) {
        this.appNotificationService.notify("Please upload the subject data CSV file or key in subject data", "danger")
        callback(false)
      } else {
        this.appNetworkService.studentTeacherMappingFileUpload(this.uploadedFile, this.school.id)
        .then(d => {
          this.appNotificationService.notify("Congratulations! Your school has been successfully created", "info")
          callback(true);
        })
        .catch(e => {
          if(e.status >= 500) {
            this.appNotificationService.notifyGenericError()
          }else if(e.status == 400 ) {
            var error = JSON.parse(e._body).map.error
            this.appNotificationService.notify(error,"danger")
          } else if(e.status == 401){
            this.appNetworkService.deleteAllCookies()
            window.location.reload();
          }
          callback(false)
        })
      }
    } else {
      this.appNetworkService.studentTeacherMappingSave(this.classInfo)
      .then(d => {
        this.appNotificationService.notify("Congratulations! Your school has been successfully created", "info")
        callback(true);
      })
      .catch(e => {
        if(e.status >= 500) {
          this.appNotificationService.notifyGenericError()
        }else if(e.status == 400 ) {
          var error = JSON.parse(e._body).map.error
          this.appNotificationService.notify(error,"danger")
        } else if(e.status == 401){
          this.appNetworkService.deleteAllCookies()
          window.location.reload();
        }
        callback(false)
      })
    }
  }

  fetchSchoolDetailsForAdmin() {
    this.appNetworkService.getSchool(this.school.id)
    .then(d => {
      var data = d.json();
      this.school.id = data.id
      this.school.name = data.name;
      this.school.stage = data.stage;
      this.adminId = data.currentUserAdminId;
      this.school.classList = data.classList

      if(!this.appNetworkService.getCookie("roleauth")) {
        this.appNetworkService.getRoleAuthKey("admin", this.adminId, this.school.id)
        .then(dt => {
          if(!dt){
            this.appNotificationService.notifyGenericError()
            window.location.reload();
          } else {
            this.isDataAvailable = true;
          }
        })
        .catch(ex => {
          if(ex.status >= 500) {
            this.appNotificationService.notifyGenericError()
          }else if(ex.status == 400 ) {
            this.appNotificationService.notify("Please fill in all required fields correctly.","danger")
          } else if(ex.status >= 401){
            this.appNetworkService.deleteAllCookies()
            window.location.reload();
          }
        })
      } else {
        this.isDataAvailable = true;
      }
    })
    .catch(e => {
      if(e.status >= 500) {
        this.appNotificationService.notifyGenericError()
      } else if(e.status == 400 ) {
        this.appNotificationService.notify("Please fill in all required fields correctly.","danger")
      } else if(e.status >= 401){
        this.appNetworkService.deleteAllCookies()
        window.location.reload();
      }
    })
  }

  ngOnInit() {
    this.isDataAvailable = false;
    this.uploadedFile = null;
    if(this.router.url !== "/schoolCreate") {
      if(!this.school.id) {
        this.route.params.subscribe(params => {
          this.school.id =params['id'];
        })
      }
      this.fetchSchoolDetailsForAdmin()
    } else {
      this.isDataAvailable = true;
    }
  }

}
