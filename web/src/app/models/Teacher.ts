import { User } from './User';

export class Teacher {
	id: number;
	user: User;
	name: string;
	employeeId: string;
	ctClass;
}