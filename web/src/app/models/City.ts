import { State } from './State';
import { Locality } from './Locality';

export class City {
	readonly id: number;
	readonly code: string;
	readonly name: string;
	readonly state: State;
	localityList: Locality[]
}