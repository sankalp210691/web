import { City } from './City';

export class Locality {
	readonly id: number;
	readonly name: string;
	readonly City: City;
}