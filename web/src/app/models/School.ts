import { Board } from './Board';
import { State } from './State';
import { Class } from './Class';
import { InstituteCategory } from './InstituteCategory';
import { Locality } from './Locality';

export class School {
  	public id: number;
  	public displayPic: string
  	public displayPicImage: File
    public name: string;
    public address: string;
    public pincode: string;
    public boardList: Board[];
    public email: string;
    public phone: string;
    public stage: number = 0;
    public classList: [Class];
    public locality: Locality;
    public instituteCategoryList: InstituteCategory[]
    public establishmentYear: string
    public admissionsOpen: boolean
}