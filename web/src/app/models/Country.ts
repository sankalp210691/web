import { State } from './State'

export class Country {
	readonly id: number
	readonly name: string
	readonly code: string
	readonly internationalPhoneCode: string
	stateList: State[]
}