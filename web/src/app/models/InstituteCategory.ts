export class InstituteCategory {
	readonly id: number
	readonly name: string
}