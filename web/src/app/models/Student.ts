import { Class } from './Class';
import { Parent } from './Parent';

export class Student {
	id: number;
	name: string;
	profilePicUrl;
	clazz: Class
	parent:[Parent]
	gender: string
	dob: string
	admissionCode: string
}