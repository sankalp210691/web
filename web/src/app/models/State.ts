import { Country } from './Country';
import { City } from './City';

export class State {
	readonly id: number
	readonly name: string
	readonly country: Country
	cityList: City[]
}