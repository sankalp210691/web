import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpModule } from "@angular/http";
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { MatTabsModule } from '@angular/material';
import { MatTableDataSource } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatStepperModule } from '@angular/material/stepper';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { MatListModule } from '@angular/material/list';
import { CookieService, CookieOptions, BaseCookieOptions } from 'angular2-cookie';
import { ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DatePipe } from '@angular/common';
import { ImageCropperModule } from 'ngx-image-cropper';

import { AppRoutingModule } from './app.routing';
import { AppNetworkService } from './services/app-network.service';
import { AppNotificationService } from './services/app-notification.service';
import { SearchFilterPipe } from './pipes/SearchFilterPipe';
import { SchoolTypeSetting } from './search-result/searchResult.component';
import { SchoolBoardSetting } from './search-result/searchResult.component';
import { OtherFiltersSetting } from './search-result/searchResult.component';
import { FacilityFiltersSetting } from './search-result/searchResult.component';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SettingsComponent } from './components/settings/settings.component';
import { OfflineApplyToSchoolComponent } from './components/offlineapplytoschool/offlineApplyToSchool.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SearchComponent } from './search/search.component';
import { SearchResultComponent } from './search-result/searchResult.component';
import { TagmakerComponent } from './components/tagmaker/tagmaker.component';
import { SchoolCreationWizardComponent } from './school-creation-wizard/school-creation-wizard.component';
import { SchoolInfoComponent } from './components/schoolinfo/schoolinfo.component';
import { ClassInfoComponent } from './components/classinfo/classinfo.component';
import { StudentInputComponent } from './components/studentinput/studentinput.component';
import { StudentInputFormComponent } from './components/studentinputform/studentinputform.component';
import { TeacherInputComponent } from './components/teacherinput/teacherinput.component';
import { SubjectInputComponent } from './components/subjectinput/subjectinput.component';
import { SchoolSearchComponent } from './components/schoolsearch/schoolsearch.component';
import { RatingComponent } from './components/rating/rating.component';
import { UploaderComponent } from './components/uploader/uploader.component';
import { TeacherInputFormComponent } from './components/teacherinputform/teacherinputform.component';
import { ConfirmComponent } from './services/app-notification.service';
import { ProfilePicComponent } from './components/profilepic/profilepic.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SchoolProfileComponent } from './school-profile/school-profile.component';
import {
  AgmCoreModule
} from '@agm/core';
import { SubjectinputformComponent } from './components/subjectinputform/subjectinputform.component';
import { ImageEditorComponent } from './components/image-editor/image-editor.component';
import { RatingReviewComponent } from './components/rating-review/rating-review.component';
import { RatingReviewBlockComponent } from './components/rating-review-block/rating-review-block.component';

const appInitializerFn = (appNetworkService: AppNetworkService) => {
  if(appNetworkService.verifyIfLoggedIn()){
    return () => {
      return appNetworkService.getUserAsync();
    };
  }else {
    return() => {return true}
  }
};

@NgModule({
  imports: [
  BrowserAnimationsModule,
  FormsModule,
  RouterModule,
  AppRoutingModule,
  MatIconModule,
  MatListModule,
  MatButtonToggleModule,
  BrowserModule,
  MatChipsModule,
  MatTabsModule,
  HttpModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatDialogModule,
  MatToolbarModule,
  MatSidenavModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatAutocompleteModule,
  MatRadioModule,
  MatExpansionModule,
  MatTableModule,
  MatStepperModule,
  MatCheckboxModule,
  MatCardModule,
  MatSelectModule,
  MatPaginatorModule,
  ReactiveFormsModule,
  ImageCropperModule,
  AgmCoreModule.forRoot({
    apiKey: 'AIzaSyCtl4fMdUBFO7ox-FNUwR3-BDa-o1vuplg'
  })
  ],
  entryComponents: [ StudentInputFormComponent, ConfirmComponent, TeacherInputFormComponent, LoginComponent, RatingReviewComponent, OfflineApplyToSchoolComponent, SchoolInfoComponent, SettingsComponent, SubjectinputformComponent, ImageEditorComponent ],
  declarations: [
  AppComponent,
  LoginComponent,
  SettingsComponent,
  OfflineApplyToSchoolComponent,
  StudentInputFormComponent,
  TeacherInputFormComponent,
  ConfirmComponent,
  ProfilePicComponent,
  SearchComponent,
  SearchResultComponent,
  SchoolCreationWizardComponent,
  DashboardComponent,
  SearchComponent,
  SearchResultComponent,
  SchoolCreationWizardComponent,
  UserProfileComponent,
  SchoolProfileComponent,
  SchoolSearchComponent,
  SchoolInfoComponent,
  NavbarComponent,
  TagmakerComponent,
  SidebarComponent,
  RatingComponent,
  UploaderComponent,
  StudentInputComponent,
  ClassInfoComponent,
  TeacherInputComponent,
  SearchFilterPipe,
  SubjectInputComponent,
  SubjectinputformComponent,
  ImageEditorComponent,
  RatingReviewComponent,
  RatingReviewBlockComponent
  ],
  providers: [
  DatePipe,
  CookieService,
  AppNotificationService,
  AppNetworkService,
  AppNetworkService,
  SchoolTypeSetting,
  SchoolBoardSetting,
  OtherFiltersSetting,
  FacilityFiltersSetting,
  SearchFilterPipe,
  SchoolInfoComponent,
  ClassInfoComponent,
  StudentInputComponent,
  {
    provide: APP_INITIALIZER,
    useFactory: appInitializerFn,
    multi: true,
    deps: [AppNetworkService]
  },
  { provide: CookieOptions, useValue: BaseCookieOptions }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
