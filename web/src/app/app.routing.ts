import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppNetworkService } from './services/app-network.service';

import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SearchComponent } from './search/search.component';
import { SearchResultComponent } from './search-result/searchResult.component';
import { SchoolCreationWizardComponent } from './school-creation-wizard/school-creation-wizard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SchoolProfileComponent } from './school-profile/school-profile.component';

const routes: Routes =[
{ path: '',                       component: SearchComponent },
{ path: 'schools',                component: SearchResultComponent },
{ path: 'login',                  component: LoginComponent },
// { path: 'schoolCreate',           component: SchoolCreationWizardComponent },
// { path: 'schoolCreate/:id',        component: SchoolCreationWizardComponent },
// { path: 'user-profile',           component: UserProfileComponent },
// { path: 'dashboard',              component: DashboardComponent },
{ path: 'school/:id',              component: SchoolProfileComponent },
{ path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
  CommonModule,
  BrowserModule,
  RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
